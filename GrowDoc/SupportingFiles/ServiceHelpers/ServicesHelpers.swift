//
//  ServicesHelpers.swift
//  GameFame
//
//  Created by volivesolutions on 10/09/19.
//  Copyright © 2019 Prashanth. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SCLAlertView
import CRNotifications
import Kingfisher
class  ServicesHelper : NSObject {
    
    static let sharedInstance = ServicesHelper()
    
    //MARK:- GET SERVICE CALL
    func getDataServiceCall(url:String,postDict:[String:AnyObject], completionHandler : @escaping (Dictionary<String, Any>, NSError?) -> ()){
        Alamofire.request(url, method: .get, parameters: postDict).responseJSON { response in
            print("url ",url)
            print(response)
            completionHandler(((response.result.value as? Dictionary<String, Any>)!), response.result.error as NSError?)
            
            #if DEBUG
            
            
            #endif
        }
    }
    
    //MARK:- POST SERVICE
    func postServiceCall(url:String,PostDetails: [String:AnyObject], completionHandler : @escaping (Dictionary<String, Any>, NSError?) -> ()){
        print("loginu",url)
        print("logind",PostDetails)
        Alamofire.request(url, method: .post, parameters: PostDetails, encoding: URLEncoding.default, headers: nil).responseJSON { response in
            print(response)
            
            completionHandler(response.result.value as! Dictionary<String, Any>, response.result.error as NSError?)
            #if DEBUG
            
            
            #endif
        }
        //        Alamofire.request(url, method: .post, parameters: PostDetails,encoding:
        //            JSONEncoding.default, headers: nil).responseJSON {
        //                response in
        //                switch response.result {
        //                case .success:
        //                    print(response)
        //                    break
        //
        //                case .failure(let error):
        //                    print(error)
        //                }
        //        }
    }
    
    var errMessage: String!
    let imageV = UIImageView()
    let indicator: UIActivityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
    
    func loader(view: UIView) -> () {
        indicator.frame = CGRect(x: 0,y: 0,width: 75,height: 75)
        indicator.layer.cornerRadius = 8
        indicator.center = view.center
        indicator.color = UIColor.init(red: 84.0/255.0, green: 144.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        view.addSubview(indicator)
        indicator.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        indicator.bringSubviewToFront(view)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        indicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func dissMissLoader()  {
        indicator.stopAnimating()
        imageV.removeFromSuperview()
        UIApplication.shared.endIgnoringInteractionEvents()
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func setCircleForImage(_ imageView : UIImageView){
        imageView.layer.cornerRadius = imageView.frame.size.width/2
        imageView.clipsToBounds = true
    }
    
 
}

extension UIImageView {
   func setKFImage(with urlString: String){
     guard let url = URL.init(string: urlString) else {
       return
     }
     let resource = ImageResource(downloadURL: url, cacheKey: urlString)
     let kf = self.kf
     kf.indicatorType = .activity
     self.kf.setImage(with: resource)
   }
   func setCircleForImage(){
     self.layer.cornerRadius = self.frame.size.width/2
     self.clipsToBounds = true
   }
   func roundedImageWithWiteCornerRadius() {
         self.layer.cornerRadius = (self.frame.size.width) / 2;
         self.clipsToBounds = true
         self.layer.borderWidth = 3.0
         self.layer.borderColor = UIColor.white.cgColor
       }
 }

extension UIView {
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
}

//// Validation Toasts
extension UIViewController {
    
        func showToast(message : String) {
            CRNotifications.showNotification(type: CRNotifications.success, title: self.languageChangeString(a_str: "Success!")!, message: message, dismissDelay: 4, completion: {
            })
//            let appearance = SCLAlertView.SCLAppearance(
//                showCircularIcon: true
//            )
//            let alertView = SCLAlertView(appearance: appearance)
//            //let alertViewIcon = UIImage(named: "Success") //Replace the IconImage text with the image name
//           // alertView.showInfo( "Alert!" , subTitle: message ,closeButtonTitle: "OK" , circleIconImage: alertViewIcon)
//            alertView.showTitle("Success", subTitle: message, style: SCLAlertViewStyle.success)
        }
    
        func showToastForAlert(message : String) {
            CRNotifications.showNotification(type: CRNotifications.error, title: self.languageChangeString(a_str: "Alert!")!, message: message, dismissDelay: 4)
//            let appearance = SCLAlertView.SCLAppearance(
//                showCircularIcon: true
//            )
//            let alertView = SCLAlertView(appearance: appearance)
//           // let alertViewIcon = UIImage(named: "Failure") //Replace the IconImage text with the image name
//            alertView.showError("Alert!", subTitle: message, style: SCLAlertViewStyle.error)
            

    }
    //MARK:SHOW TOAST FOR INTERNET
    func showToastForInternet (message : String) {
        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        self.present(alert, animated: true)
       // duration in seconds
        let duration: Double = 3
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    /*
    
    //MARK:SHOW TOAST
    func showToast(message : String) {
        
        let message = message
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        
        self.present(alert, animated: true)
        
        let duration: Double = 2
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration) {
            alert.dismiss(animated: true)
        }
    }
    */
    
    //MARK:LANGUAGE CHANGE STRING
    func languageChangeString(a_str: String) -> String?{
        
        var path: Bundle?
        var selectedLanguage:String = String()
        //UserDefaults.standard.set(ARABIC_LANGUAGE, forKey: "currentLanguage")
        // selectedLanguage = UserDefaults.standard.integer(forKey: )
        selectedLanguage = UserDefaults.standard.string(forKey: "currentLanguage") ?? "en"
        
        let aType = Bundle.main.path(forResource: selectedLanguage, ofType: "lproj")
        path = Bundle(path: aType!)
        
        
        
        
//        if selectedLanguage == ENGLISH_LANGUAGE {
//            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
//                path = Bundle(path: aType)!
//            }
//        }
//        else if selectedLanguage == FRENCH_LANGUAGE {
//            if let aType = Bundle.main.path(forResource: "fr", ofType: "lproj") {
//                path = Bundle(path: aType)!
//            }
//        }
//        else {
//            if let aType = Bundle.main.path(forResource: "en", ofType: "lproj") {
//                path = Bundle(path: aType)!
//            }
//        }
        // var languageBundle = Bundle(path: path)
        
        let str: String = NSLocalizedString(a_str, tableName: "LocalizableGrowDoc", bundle: path!, value: "", comment: "")
        
        return str
    }
    
}

extension String {
    
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return self.substring(to: range.lowerBound)
        }
        
        return self.substring(from: range.upperBound)
    }
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
