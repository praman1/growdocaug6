//
//  APIConstants.swift
//  GrowDoc
//
//  Created by volivesolutions on 18/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation

let ENGLISH_LANGUAGE = "en"
let FRENCH_LANGUAGE = "fr"
let DEVICE_TOKEN = UserDefaults.standard.object(forKey: "deviceToken")
let DEVICE_TYPE = "iOS"
let USERID = UserDefaults.standard.object(forKey: "userId") as? String ??  ""

//let BASE_URL = "https://growdoc.net/services/"

 let  BASE_URL  = "http://demomaplebrains.com/growdoc_phase/services/"
//"https://www.demomaplebrains.com/growdoc_dev/services/"
//let BASE_PATH = "https://growdoc.net/"
 let BASE_PATH = "http://demomaplebrains.com/growdoc_phase/"
//"https://www.demomaplebrains.com/growdoc_dev/"
let registrationAPI = "\(BASE_URL)registration?"
let loginAPI = "\(BASE_URL)login?"
let forgotPasswordAPI = "\(BASE_URL)forgot_pwd?"
let termsConditionsAPI = "\(BASE_URL)terms_conditions"
let privacyPolicyAPI = "\(BASE_URL)privacy_policy"
let aboutUsAPI = "\(BASE_URL)about_us"
let contactUsAPI = "\(BASE_URL)contact_us?"
let blogAPI = "\(BASE_URL)blog"
let blogDetailsAPI = "\(BASE_URL)blog_details?"
let blogLikesAPI = "\(BASE_URL)blog_likes?"
let blogCommentsAPI = "\(BASE_URL)blog_comments?"
let galleryAPI = "\(BASE_URL)gallery?"
let galleryDetailsAPI = "\(BASE_URL)gallery_details?"
let onboardingAPI = "\(BASE_URL)onboarding?"
let profileAPI = "\(BASE_URL)profile?"
let updateProfileAPI = "\(BASE_URL)update_profile?"
let relatedImagesAPI = "\(BASE_URL)related_images"
let socialAPI = "\(BASE_URL)social"
let logoutAPI = "\(BASE_URL)logout?"
let historyAPI = "\(BASE_URL)history?"
let bannersAPI = "\(BASE_URL)banners"
let membershipPlansAPI = "\(BASE_URL)membership_plans?"
let purchaseMembershipAPI = "\(BASE_URL)purchaseMembership?"
let userMembershipsAPI = "\(BASE_URL)userMemberships?"
let membershipCheckAPI = "\(BASE_URL)membershipCheck?"
let showAds = "\(BASE_URL)show_ads?"
let scanInstructions = "\(BASE_URL)scan_instructions?"
let membership_popup = "\(BASE_URL)membership_popup?"
//Rami Added New service
let DISEASES_RELATED_DATA_API = "\(BASE_URL)related_data"

