//
//  AppConstants.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation
import UIKit

let onBoardheadersArray = ["Take Picture","Find Solutions","Success"]
let onBoardTitlesArray = ["Take a plant Picture and upload","Find out solutions and recommendations","Read Solutions and Succeed you Crops"]
let onBoardSubTitlesArray = ["Take a Picture of your plant and GrowDoc will provide with instant solutions","GrowDoc Find out what's happening to your plant","Identify plant diseases ,pests and nutrient deficiencies.Empowers to control them and increase your yield"]
let onBoardimagesArray = [#imageLiteral(resourceName: "Group 3941"),#imageLiteral(resourceName: "Group 3942"),#imageLiteral(resourceName: "Group 3943")]

//let menuNamesArray = ["Home","About Us","Blog","Gallery","Terms and Conditions","Privacy Policy","Contact","","","Logout"]


let blogPostImagesArray = ["light-burn","nutrient-burn","Spider-Mites","White-Powdery-Mildew","phfluctpicsquareforsite","over-watring"]
let detailsPageSimilarImagesArray = [#imageLiteral(resourceName: "light-burn-leaf-cannabis"),#imageLiteral(resourceName: "cannabis-heat-stress-light-burn-bleaching-plus-spots-on-leaves-symptoms"),#imageLiteral(resourceName: "Group 3984")]
let plantsImagesArray = ["light-burn","nutrient-burn","Spider-Mites","White-Powdery-Mildew","phfluctpicsquareforsite","over-watring"]
let plantsNames = ["Light Burn","Nutrient Burn","Spider Mites","White Powdery Mildew","PH Issue","Over Watering"]
var detailsPagesSimilarVedioArrays = [#imageLiteral(resourceName: "Group 3983"),#imageLiteral(resourceName: "Group 3982"),#imageLiteral(resourceName: "Group 3982")]

let appColor = UIColor.init(red: 141.0/255.0, green: 200.0/255.0, blue: 63.0/255.0, alpha: 1)

extension UIButton {
    func setBorder() {
        self.layer.borderColor = UIColor.init(red: 141.0/255.0, green: 200.0/255.0, blue: 63.0/255.0, alpha: 1).cgColor
        self.layer.borderWidth = 2.0
    }
}


extension UIViewController{
    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
 }
}
struct AppFont {
    
    static func semiBold(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-SemiBold", size: size) {
            return font
        }
        fatalError("Could not load bold Italic font")
    }
    
    static func medium(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Medium", size: size) {
            return font
        }
        fatalError("Could not load bold Italic font")
    }
    
 
    static func regular(size: CGFloat) -> UIFont {
        if let font = UIFont(name: "Poppins-Regular", size: size) {
            return font
        }
        fatalError("Could not load regular font")
    }
    
}
