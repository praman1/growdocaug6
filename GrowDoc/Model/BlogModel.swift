//
//  BlogModel.swift
//  GrowDoc
//
//  Created by volivesolutions on 20/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation
class BlogModel{

    var blogId : String!
    var image : String!
    var title : String!
    var description : String!
    var date : String!
    init(blogData : NSDictionary) {
        if let id = blogData["id"]{
            self.blogId = id as? String ?? ""
        }
        if let image = blogData["image"]{
            self.image = image as? String ?? ""
        }
        if let title = blogData["title"]{
            self.title = title as? String ?? ""
        }
        if let description = blogData["description"]{
            self.description = description as? String ?? ""
        }
        if let date = blogData["date"]{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "d MMM yyyy"
            
            let finalDate: Date? = dateFormatterGet.date(from: date as! String)
            let convertedDate = dateFormatter.string(from: finalDate!)
            self.date = convertedDate
        }
    }
}
