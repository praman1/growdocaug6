//
//  GalleryModel.swift
//  GrowDoc
//
//  Created by volivesolutions on 26/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation
class GalleryModel{

    var galleryId : String!
    var image : String!
    var title : String!
    init(galleryData : NSDictionary) {
        if let id = galleryData["id"]{
            self.galleryId = id as? String ?? ""
        }
        if let image = galleryData["image"]{
            self.image = image as? String ?? ""
        }
        if let title = galleryData["title"]{
            self.title = title as? String ?? ""
        }
    }
}
