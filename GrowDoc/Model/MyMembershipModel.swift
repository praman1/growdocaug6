//
//  MyMembershipModel.swift
//  GrowDoc
//
//  Created by Apple on 13/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import Foundation
class MyMembershipModel{
    var id : String!
    var membership_name : String!
    var expired_date : String!
    var duration : String!
    var price : String!
    var expires_in : String!
    var scans_left : String!
    var status : String!
    var amount : String!
    var contact_support : String!
    var custom_text : String!
    init(membershipData : NSDictionary) {
        if let id = membershipData["id"]{
            self.id = id as? String ?? ""
        }
        if let name = membershipData["membership_name"]{
            self.membership_name = name as? String ?? ""
        }
        if let expired_date = membershipData["expired_date"]{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd MMMM yyyy"
            
            let finalDate: Date? = dateFormatterGet.date(from: expired_date as! String)
            let convertedDate = dateFormatter.string(from: finalDate!)
            //self.date_of_added = convertedDate
            self.expired_date = convertedDate
        }
        if let duration = membershipData["duration"]{
            self.duration = duration as? String ?? ""
        }
        if let price = membershipData["price"]{
            self.price = price as? String ?? ""
        }
        if let expires_in = membershipData["expires_in"]{
            self.expires_in = expires_in as? String ?? ""
        }
        if let amount = membershipData["amount"]{
            self.amount = amount as? String ?? ""
        }
        if let scans_left = membershipData["scans_left"]{
            self.scans_left = scans_left as? String ?? ""
        }
        if let status = membershipData["status"]{
            self.status = status as? String ?? ""
        }
        if let contactSupport = membershipData["contact_support"]{
            self.contact_support = contactSupport as? String ?? ""
        }
        if let customText = membershipData["custom_text"]{
            self.custom_text = customText as? String ?? ""
        }
    }
}
