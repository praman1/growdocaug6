//
//  HistoryModel.swift
//  GrowDoc
//
//  Created by volivesolutions on 10/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation
class HistoryModel{
    
    var image : String!
    var created_date : String!
    var gallery_id : String!
    init(histotyData : NSDictionary) {
        if let image = histotyData["image"]{
            self.image = image as? String ?? ""
        }
        if let galleryId = histotyData["gallery_id"]{
            self.gallery_id = galleryId as? String ?? ""
        }
        if let date = histotyData["created_date"]{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
            
            let finalDate: Date? = dateFormatterGet.date(from: date as! String)
            let convertedDate = dateFormatter.string(from: finalDate!)
            self.created_date = convertedDate
        }
    }
}
