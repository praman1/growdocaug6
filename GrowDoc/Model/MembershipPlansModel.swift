//
//  MembershipPlansModel.swift
//  GrowDoc
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import Foundation
class MembershipPlansModel{
    var membership_id : String!
    var membership_name : String!
    var price : String!
    var duration : String!
    var scans : String!
    var contact_support : String!
    var custom_text : String!
    init(plansData : NSDictionary) {
        if let id = plansData["membership_id"]{
            self.membership_id = id as? String ?? ""
        }
        if let name = plansData["membership_name"]{
            self.membership_name = name as? String ?? ""
        }
        if let price = plansData["price"]{
            self.price = price as? String ?? ""
        }
        if let duration = plansData["duration"]{
            self.duration = duration as? String ?? ""
        }
        if let scans = plansData["scans"]{
            self.scans = scans as? String ?? ""
        }
        if let contactSupport = plansData["contact_support"]{
            self.contact_support = contactSupport as? String ?? ""
        }
        if let customText = plansData["custom_text"]{
            self.custom_text = customText as? String ?? ""
        }
    }
}
