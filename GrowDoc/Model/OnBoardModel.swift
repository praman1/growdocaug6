//
//  OnBoardModel.swift
//  GrowDoc
//
//  Created by volivesolutions on 19/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation

class OnBoardModel{

    var image : String!
    var title : String!
    var sub_title : String!
    var description : String!
    
    init(onBoardData : NSDictionary) {
        if let image = onBoardData["image"]{
            self.image = image as? String ?? ""
        }
        if let title = onBoardData["title"]{
            self.title = title as? String ?? ""
        }
        if let subTitle = onBoardData["sub_title"]{
            self.sub_title = subTitle as? String ?? ""
        }
        if let description = onBoardData["description"]{
            self.description = description as? String ?? ""
        }
    }
}

class BannerModel{
    var id : String!
    var image : String!
    init(bannerData : NSDictionary) {
        if let id = bannerData["id"]{
            self.id = id as? String ?? ""
        }
        if let image = bannerData["image"]{
            let bannerImage : String = image as! String
            self.image = BASE_PATH + bannerImage
        }
    }
}
