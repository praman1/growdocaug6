//
//  CommentsModel.swift
//  GrowDoc
//
//  Created by volivesolutions on 29/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import Foundation
class CommentsModel{

    var userName : String!
    var comment : String!
    var created_date : String!
    var reply : String!
    var reply_date : String!
    init(commentData : NSDictionary) {
        if let name = commentData["username"]{
            self.userName = name as? String ?? ""
        }
        if let comment = commentData["comment"]{
            self.comment = comment as? String ?? ""
        }
        if let reply = commentData["reply"]{
            self.reply = reply as? String ?? ""
        }
        if let date = commentData["comment_date"]{
            let dateFormatterGet = DateFormatter()
            dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy h:mm a"
            
            let finalDate: Date? = dateFormatterGet.date(from: date as! String)
            let convertedDate = dateFormatter.string(from: finalDate!)
            self.created_date = convertedDate
        }
        if let replyDate = commentData["reply_date"]{
            let dateFormatterGet1 = DateFormatter()
            dateFormatterGet1.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateFormat = "dd-MM-yyyy h:mm a"
            
            let finalDate1: Date? = dateFormatterGet1.date(from: replyDate as! String)
            let convertedDate1 = dateFormatter1.string(from: finalDate1!)
            self.reply_date = convertedDate1
        }
    }
}
