//
//  SideMenuVC.swift
//  GrowDoc
//
//  Created by volive solutions on 29/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import Alamofire
class SideMenuVC: UIViewController {

    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet var languageBtn: UIButton!
    var menuNamesArray = [String]()
    var window : UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        /*
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.string(forKey: "currentLanguage") == ENGLISH_LANGUAGE{
                self.languageBtn.tag = 2
                self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
            }else{
                self.languageBtn.tag = 1
                self.languageBtn.setTitle("En", for: UIControl.State.normal)
            }
        }
        */
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                 self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
                self.languageBtn.tag = 1
            }else{
                 self.languageBtn.setTitle("En", for: UIControl.State.normal)
                self.languageBtn.tag = 0
            }
        }else{
            self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
            self.languageBtn.tag = 1
        }
        //,self.languageChangeString(a_str: "Contact")
        if UserDefaults.standard.object(forKey: "userId") != nil{
            self.menuNamesArray = [self.languageChangeString(a_str: "Home"),self.languageChangeString(a_str: "My Membership"),self.languageChangeString(a_str: "About Us"),self.languageChangeString(a_str: "Blog"),self.languageChangeString(a_str: "View Plant Problems"),self.languageChangeString(a_str: "Terms and Conditions"),self.languageChangeString(a_str: "Privacy Policy"),self.languageChangeString(a_str: "Profile"),self.languageChangeString(a_str: "History"),"",self.languageChangeString(a_str: "Logout")] as! [String]
        }else{
            self.menuNamesArray = [self.languageChangeString(a_str: "Home"),self.languageChangeString(a_str: "About Us"),self.languageChangeString(a_str: "Blog"),self.languageChangeString(a_str: "View Plant Problems"),self.languageChangeString(a_str: "Terms and Conditions"),self.languageChangeString(a_str: "Privacy Policy"),"",self.languageChangeString(a_str: "Login")] as! [String]
        }
        self.menuTableView.reloadData()
        //,self.languageChangeString(a_str: "Contact")
        
//        if UserDefaults.standard.bool(forKey: "isGuest") == true{
//            self.menuNamesArray = ["Home","About Us","Blog","Gallery","Terms and Conditions","Privacy Policy","Contact","","Login"]
//        }else{
//            if UserDefaults.standard.object(forKey: "userId") != nil{
//                self.menuNamesArray = ["Home","About Us","Blog","Gallery","Terms and Conditions","Privacy Policy","Contact","Profile","History","","Logout"]
//            }else{
//                self.menuNamesArray = ["Home","About Us","Blog","Gallery","Terms and Conditions","Privacy Policy","Contact","","Login"]
//            }
//        }
    }
    
    @IBAction func languageBtnAction(_ sender: Any) {
        if self.languageBtn.tag == 0{
            self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
            self.languageBtn.tag = 1
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(homeVC, animated: true)
            UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")
            NotificationCenter.default.post(name: NSNotification.Name("reloadTabBar"), object: nil)
        }else{
            self.languageBtn.setTitle("En", for: UIControl.State.normal)
            self.languageBtn.tag = 0
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(homeVC, animated: true)
            UserDefaults.standard.set(FRENCH_LANGUAGE, forKey: "currentLanguage")
            NotificationCenter.default.post(name: NSNotification.Name("reloadTabBar"), object: nil)
        }
    }
    
    //MARK: LOGOUT SERVICE CALL
    func logoutServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/logout?user_id=__
            let postDict = ["user_id" : USERID]
            ServicesHelper.sharedInstance.getDataServiceCall(url: logoutAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    UserDefaults.standard.set(false, forKey: "Loggedin")
                    UserDefaults.standard.set(false, forKey: "isGuest")
                    UserDefaults.standard.set(false, forKey: "membership")
                    UserDefaults.standard.removeObject(forKey: "userId")
                    UserDefaults.standard.removeObject(forKey: "username")
                    UserDefaults.standard.removeObject(forKey: "currentLanguage")
                    UserDefaults.standard.set(false, forKey: "doNotshowgender")
                    UserDefaults.standard.set(false, forKey: "doNotshowmeter")
                    UserDefaults.standard.set(false, forKey: "doNotshowscan")
                   // UserDefaults.standard.removeObject(forKey: "deviceToken")
                    //6E2CD60FDE771250434FF73080A29B8FFF8E4AC1EE9E862368ABBAEAF0C7A796
                    let story = UIStoryboard.init(name: "Main", bundle: nil)
                    let startVC = story.instantiateViewController(withIdentifier: "StartVC") as! StartVC
                    startVC.tabBarController?.hidesBottomBarWhenPushed = false
                    self.navigationController?.pushViewController(startVC, animated: true)
                    
                }else{
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                    ServicesHelper.sharedInstance.dissMissLoader()
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
        
    }
}

extension SideMenuVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return menuNamesArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SideMenuCell
        cell.title_lbl?.text = menuNamesArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("did select index",indexPath.row)
        if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Home"){
            
           // if UserDefaults.standard.object(forKey: "userId") != nil{
               // self.dismiss(animated: true, completion: nil)
//            let home = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//            home.selectedIndex = 0
//            self.navigationController?.pushViewController(home, animated: true)
            
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let homeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                //homeVC.selectedIndex = 0
                self.navigationController?.pushViewController(homeVC, animated: true)
            
        }else if  menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "My Membership"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "MyMembershipVC") as! MyMembershipVC
            self.navigationController?.pushViewController(blogVC, animated: true)
        }else if  menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "About Us"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "AboutVCViewController") as! AboutVCViewController
            self.navigationController?.pushViewController(blogVC, animated: true)
            
        }else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Blog"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "BlogPostVC") as! BlogPostVC
            blogVC.fromSideMenu = "Yes"
            self.navigationController?.pushViewController(blogVC, animated: true)
            
        }
        else if  menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "View Plant Problems") {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
            blogVC.fromSideMenu = "Yes"
            self.navigationController?.pushViewController(blogVC, animated: true)
            
        }else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Terms and Conditions"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "AboutVCViewController") as! AboutVCViewController
                blogVC.fromVC = "Terms"
            self.navigationController?.pushViewController(blogVC, animated: true)
            
        }
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Privacy Policy"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "PrivacyPoliceVC") as! PrivacyPoliceVC
            self.navigationController?.pushViewController(blogVC, animated: true)
            
        }
            /*
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Contact") {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let blogVC = story.instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
            blogVC.sideCheckString = "fromSide"
            // blogVC.tabBarController?.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(blogVC, animated: true)
        }
            */
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Profile") {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let profileVC = story.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            // blogVC.tabBarController?.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "History") {
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let historyVC = story.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
            // blogVC.tabBarController?.hidesBottomBarWhenPushed = true
            historyVC.sideCheckString = "fromSide"
            self.navigationController?.pushViewController(historyVC, animated: true)
        }
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Logout"){
            self.logoutServiceCall()
        }
        else if menuNamesArray[indexPath.row] == self.languageChangeString(a_str: "Login"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let SignInVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
            SignInVC.guestCheckString = "guest"
            self.navigationController?.pushViewController(SignInVC, animated: true)
            print("Guest Login")
        }
    }
}
