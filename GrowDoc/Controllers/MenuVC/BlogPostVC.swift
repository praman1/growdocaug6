//
//  BlogPostVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class BlogPostVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var blogTableView: UITableView!
    var fromSideMenu:String!
    var languageString : String! = ""
    var blogsPostArray = [BlogModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.languageChangeString(a_str: "Blog Posts")
        blogTableView.register(UINib(nibName: "BlogTableViewCell", bundle: nil), forCellReuseIdentifier: "BlogTableViewCell")
        blogTableView.estimatedRowHeight = 250
        self.blogServiceCall()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    
    //MARK:- MENU BTN ACTION
    @IBAction func menuBtnAction(_ sender: Any) {
        if fromSideMenu == "Yes"{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:BLOG SERVICE CALL
    func blogServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/blog?lang=
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: blogAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of blogAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                //let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.blogsPostArray = [BlogModel]()
                    if let blogsPost = responseData["data"] as? [[String : Any]]{
                        for item in blogsPost{
                            let blogsPostObject = BlogModel(blogData: item as NSDictionary)
                            self.blogsPostArray.append(blogsPostObject)
                        }
                    }
                    DispatchQueue.main.async {
                        self.blogTableView.reloadData()
                    }
                }else{
                    let message = responseData["message"] as! String
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}

extension BlogPostVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.blogsPostArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = blogTableView.dequeueReusableCell(withIdentifier: "BlogTableViewCell", for: indexPath) as! BlogTableViewCell
        let blogObject = self.blogsPostArray[indexPath.row]
        cell.titleLabel.text = blogObject.title
        cell.decriptionLabel.text = blogObject.description
        cell.blogDateLabel.text = blogObject.date
        let imageString : String = BASE_PATH + blogObject.image
        cell.blogImageView.setKFImage(with: imageString)
        //cell.blogImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogDetails = story.instantiateViewController(withIdentifier: "BlogDetailsVC") as! BlogDetailsVC
        blogDetails.blogIdString = self.blogsPostArray[indexPath.row].blogId
        blogDetails.blogImageString = self.blogsPostArray[indexPath.row].image
        self.navigationController?.pushViewController(blogDetails, animated: true )
    }
    
}
