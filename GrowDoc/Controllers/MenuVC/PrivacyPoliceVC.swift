//
//  PrivacyPoliceVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class PrivacyPoliceVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var HeaderLabel: UILabel!
    
    @IBOutlet var privacyPolicyTextView: UITextView!
    var textString : String?
    var languageString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.languageChangeString(a_str: "Privacy Policy")
        self.HeaderLabel.text = self.languageChangeString(a_str: "Privacy Policy")
        self.privacyPolicyServiceCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func backBtnAction(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    //MARK:PRIVACY POLICY SERVICE CALL
    func privacyPolicyServiceCall(){
        if Reachability.isConnectedToNetwork() {
            //ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/privacy_policy
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            let postDict = ["lang" : self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: privacyPolicyAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    if let aboutUs = responseData["data"] as? [String: Any] {
                        //let termsStr = termsData.text
                        DispatchQueue.main.async {
                            self.privacyPolicyTextView.font = UIFont(name: "Poppins-Regular", size: 16.0)
                            self.privacyPolicyTextView.textColor = UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
                        }
                        self.textString = aboutUs["privacy"] as? String
                        let attrStr = try! NSAttributedString(
                            data: (self.textString?.data(using: .unicode, allowLossyConversion: true)!)!,
                            options:[.documentType: NSAttributedString.DocumentType.html,
                                     .characterEncoding: String.Encoding.utf8.rawValue],
                            documentAttributes: nil)
                        self.privacyPolicyTextView.attributedText = attrStr
                        
                    }
                }else{
                    let message = responseData["message"] as! String
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            //ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
    }
    
}
