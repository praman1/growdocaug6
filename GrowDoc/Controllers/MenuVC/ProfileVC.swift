//
//  ProfileVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu
class ProfileVC: UIViewController {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailIdTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var userNameHeaderLabel: UILabel!
    @IBOutlet weak var emailHeaderLabel: UILabel!
    @IBOutlet weak var passwordHeaderLabel: UILabel!
    
    @IBOutlet var editBtn: UIButton!
    @IBOutlet var passwordTextField: UITextField!
    var window: UIWindow?
    var userIdString : String! = ""
    var languageString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.editBtn.tag = 0
        self.editBtn.setTitle(self.languageChangeString(a_str: "Edit"), for: UIControl.State.normal)
        self.navigationController?.isNavigationBarHidden = true
        self.passwordTextField.placeholder = self.languageChangeString(a_str: "Password")
        self.userNameHeaderLabel.text = self.languageChangeString(a_str: "User Name")
        self.emailHeaderLabel.text = self.languageChangeString(a_str: "Email")
        self.passwordHeaderLabel.text = self.languageChangeString(a_str: "Password")
        self.titleLabel.text = self.languageChangeString(a_str: "Profile")
//        if UserDefaults.standard.bool(forKey: "isGuest") == true{
//            //self.nameTextField.isUserInteractionEnabled = true
//        }else{
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            //self.nameTextField.isUserInteractionEnabled = false
            self.getProfileServiceCall()
        //}
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:GET PROFILE SERVICE CALL
    func getProfileServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/profile?user_id=72
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            
            let postDict = ["user_id":self.userIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: profileAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of profileAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    if let data = responseData["Profile"] as? [String : Any]{
                        self.userIdString = data["user_id"] as? String
                        self.nameTextField.text = data["username"] as? String
                        self.emailIdTextField.text = data["email"] as? String
                    }
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    @IBAction func editButtonAction(_ sender: Any) {
         if UserDefaults.standard.bool(forKey: "isGuest") == true{
            self.showToastForAlert(message: languageChangeString(a_str: "You're a Stay Anonymous,Please create an account")!)
            let signUp = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUp.guestCheck = "guest"
            self.navigationController?.pushViewController(signUp, animated: true)
         }else{
            if self.editBtn.tag == 0{
                self.editBtn.tag = 1
                self.editBtn.setTitle(self.languageChangeString(a_str: "Save"), for: UIControl.State.normal)
                self.nameTextField.isUserInteractionEnabled = true
                self.emailIdTextField.isUserInteractionEnabled = true
                self.passwordTextField.isUserInteractionEnabled = true
            }else{
                if self.nameTextField.text == ""{
                    self.showToastForAlert(message: languageChangeString(a_str: "Plase enter user name")!)
                }else if self.emailIdTextField.text == ""{
                    self.showToastForAlert(message: languageChangeString(a_str: "Plase enter email")!)
                }else if self.emailIdTextField.text != ""{
                    do {
                        let email = try self.emailIdTextField.validatedText(validationType: ValidatorType.email)
                        //self.emailIsString = email
                        self.updateProfileServiceCall()
                    } catch(let error) {
                        //showToast(message: (error as! ValidationError).message)
                        print(error)
                        self.showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
                    }
                }
            }
        }
    }
    
    //MARK: UPDATE PROFILE SERVICE CALL
    func updateProfileServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/update_profile?user_id=__&email=__&username=__&password=(can
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["user_id":self.userIdString,"email":self.emailIdTextField.text!,"username":nameTextField.text!,"password":self.passwordTextField.text ?? "","lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: updateProfileAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of updateProfileAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToast(message: message)
                    self.editBtn.tag = 0
                    self.editBtn.setTitle(self.languageChangeString(a_str: "Edit"), for: UIControl.State.normal)
                    self.nameTextField.isUserInteractionEnabled = false
                    self.emailIdTextField.isUserInteractionEnabled = false
                    self.passwordTextField.isUserInteractionEnabled = false
                    UserDefaults.standard.set(self.nameTextField.text!, forKey: "username")
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}
/*
 if UserDefaults.standard.bool(forKey: "isGuest") == true{
 if self.nameTextField.text == ""{
 self.showToastForAlert(message: languageChangeString(a_str: "Plase enter user name")!)
 }else if self.emailIdTextField.text == ""{
 self.showToastForAlert(message: languageChangeString(a_str: "Plase enter email")!)
 }else if self.passwordTextField.text == ""{
 self.showToastForAlert(message: languageChangeString(a_str: "Plase enter password")!)
 }else if self.emailIdTextField.text != ""{
 do {
 let email = try self.emailIdTextField.validatedText(validationType: ValidatorType.email)
 //self.emailIsString = email
 self.updateProfileServiceCall()
 } catch(let error) {
 //showToast(message: (error as! ValidationError).message)
 print(error)
 self.showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
 }
 }
 }
 */
