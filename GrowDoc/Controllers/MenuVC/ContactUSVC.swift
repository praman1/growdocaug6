//
//  ContactUSVC.swift
//  GrowDoc
//
//  Created by volive solutions on 29/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
class ContactUSVC: UIViewController,UITextViewDelegate {

    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var mobileNumberTextField: UITextField!
    @IBOutlet var messageTextView: UITextView!
    @IBOutlet var messagePlaceHolderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sendMessageBtn: UIButton!
    var scannedImage = UIImage()
    var sideCheckString : String! = ""
    var imageNameString : String! = ""
    var languageString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.languageChangeString(a_str: "Contact Us")
        self.messagePlaceHolderLabel.text = self.languageChangeString(a_str: "Your message")
        self.nameTextField.placeholder = self.languageChangeString(a_str: "Your name *")
        self.emailTextField.placeholder = self.languageChangeString(a_str: "Your Email *")
        self.mobileNumberTextField.placeholder = self.languageChangeString(a_str: "Your message")
        self.sendMessageBtn.setTitle(self.languageChangeString(a_str: "SEND MESSAGE"), for: UIControl.State.normal)
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        //  self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.messageTextView.text.count > 0 {
            self.messagePlaceHolderLabel.isHidden = true
        }else{
            self.messagePlaceHolderLabel.isHidden = false
        }
    }

    @IBAction func backBtnAction(_ sender: Any) {
        if self.sideCheckString == "fromSide"{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func sendMessageBtnAction(_ sender: Any) {
        if self.nameTextField.text == "" {
            showToastForAlert(message: self.languageChangeString(a_str: "Please enter your name")!)
        }else if self.emailTextField.text == ""{
            showToastForAlert(message: self.languageChangeString(a_str: "Please enter your email")!)
        }
//        else if self.mobileNumberTextField.text == ""{
//            showToastForAlert(message: self.languageChangeString(a_str: "Please enter phone number")!)
//        }
        else if self.emailTextField.text != ""{
            do {
                let email = try self.self.emailTextField.validatedText(validationType: ValidatorType.email)
                //self.emailIsString = email
                self.contactUsServiceCall()
                //self.postImage()
            } catch(let error) {
                //showToast(message: (error as! ValidationError).message)
                showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
            }
        }
    }
    
    //MARK:CONTACT US SERVICE CALL
    func contactUsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/contact_us?name=___&email=___&message=____&image=___
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["name":self.nameTextField.text!,"email":self.emailTextField.text!,"message":self.messageTextView.text!,"lang":self.languageString,"image":self.imageNameString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: contactUsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
 
                print("Response of contactUsAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToast(message: message)
                    self.nameTextField.text = ""
                    self.emailTextField.text = ""
                    self.messageTextView.text = ""
                    self.messagePlaceHolderLabel.isHidden = false
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    /*
    func postImage(){
        if Reachability.isConnectedToNetwork() {
        let postDict = ["name":self.nameTextField.text!,"email":self.emailTextField.text!,"message":self.messageTextView.text!,"lang":"en"]
        let imgData = self.scannedImage.jpegData(compressionQuality: 0.2) ?? Data()
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData, withName: "image",fileName: "image.jpg", mimeType: "image/jpg")
            for (key, value) in postDict {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            }
        },
                         to:contactUsAPI)
        { (result) in
            print(result)
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                upload.responseJSON { response in
                    print(response.result.value ?? "")
                    guard let responseData = response.result.value as? Dictionary<String, Any> else{
                        return
                    }
                    let status : Int = Int(responseData["status"] as! Int)
                    let message = responseData["message"] as! String
                    if status == 1{
                        ServicesHelper.sharedInstance.dissMissLoader()
                        self.showToast(message: message)
                        self.nameTextField.text = ""
                        self.emailTextField.text = ""
                        self.messageTextView.text = ""
                        self.messagePlaceHolderLabel.isHidden = false
                    }else {
                        ServicesHelper.sharedInstance.dissMissLoader()
                        self.showToastForAlert(message: message)
                    }
                }
            case .failure(let encodingError):
                print(encodingError)
                ServicesHelper.sharedInstance.dissMissLoader()
            }
        }
        } else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
                //self.showToastForAlert(message: "Please check your Internet Connection")
            }
    }
    */
}
