//
//  AboutVCViewController.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class AboutVCViewController: UIViewController {
    @IBOutlet weak var navTitle_lbl: UILabel!
    var fromVC:String!
    
    @IBOutlet var aboutAndTermsTextView: UITextView!
    var facebookLink : String! = ""
    var gmailLink : String! = ""
    var instagramLink : String! = ""
    var twitterLink : String! = ""
    var youtubeLink : String! = ""
    var textString : String?
    var languageString : String! = ""
    @IBOutlet var facebookBtn: UIButton!
    @IBOutlet var gmailBtn: UIButton!
    @IBOutlet var instaBtn: UIButton!
    @IBOutlet var twitterBtn: UIButton!
    @IBOutlet var youtubeBtn: UIButton!
    
    var socialLoginArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
    
        if fromVC == "Terms" ||  fromVC == "SignUP"{
            self.navTitle_lbl.text = self.languageChangeString(a_str: "Terms and Conditions")
            self.termsServiceCall()
        }else{
            self.navTitle_lbl.text = self.languageChangeString(a_str: "About GrowDoc")
            self.aboutUsServiceCall()
        }
        self.facebookBtn.isHidden = true
        self.gmailBtn.isHidden = true
        self.instaBtn.isHidden = true
        self.youtubeBtn.isHidden = true
        self.twitterBtn.isHidden = true
        self.socialLoginUrlServiceCall()
        // Do any additional setup after loading the view.
    }
     
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    @IBAction func backBtnAction(_ sender: Any) {
        if fromVC == "Terms"{
             present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
        else if fromVC == "SignUP"{
            self.navigationController?.popViewController(animated: true)
        }else{
              present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func facebookBtnAction(_ sender: Any) {
        guard let url = URL(string: self.facebookLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func gmailBtnAction(_ sender: Any) {
        guard let url = URL(string: self.gmailLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func instagramBtnAction(_ sender: Any) {
        guard let url = URL(string: self.instagramLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func twitterBtnAction(_ sender: Any) {
        guard let url = URL(string: self.twitterLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
        }
    }
    
    @IBAction func youtubeBtnAction(_ sender: Any) {
        guard let url = URL(string: self.youtubeLink) else { return }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url)
        } else {
            // Fallback on earlier versions
        }
    }
    //MARK:ABOUT US SERVICE CALL
    func aboutUsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            //ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/about_us
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            let postDict = ["lang" : self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: aboutUsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    if let aboutUs = responseData["data"] as? [String: Any] {
                        //let termsStr = termsData.text
                        DispatchQueue.main.async {
                            self.aboutAndTermsTextView.font = UIFont(name: "Poppins-Regular", size: 16.0)
                            self.aboutAndTermsTextView.textColor = UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
                        }
                        self.textString = aboutUs["about"] as? String
                        let attrStr = try! NSAttributedString(
                            data: (self.textString?.data(using: .unicode, allowLossyConversion: true)!)!,
                            options:[.documentType: NSAttributedString.DocumentType.html,
                                     .characterEncoding: String.Encoding.utf8.rawValue],
                            documentAttributes: nil)
                        self.aboutAndTermsTextView.attributedText = attrStr
                        
                    }
                }else{
                    let message = responseData["message"] as! String
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            //ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
    }
    
    //MARK:TERMS SERVICE CALL
    func termsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            //ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/terms_conditions
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            let postDict = ["lang" : self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: termsConditionsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    if let aboutUs = responseData["data"] as? [String: Any] {
                        //let termsStr = termsData.text
                        DispatchQueue.main.async {
                            self.aboutAndTermsTextView.font = UIFont(name: "Poppins-Regular", size: 16.0)
                            self.aboutAndTermsTextView.textColor = UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
                        }
                        self.textString = aboutUs["terms"] as? String
                        let attrStr = try! NSAttributedString(
                            data: (self.textString?.data(using: .unicode, allowLossyConversion: true)!)!,
                            options:[.documentType: NSAttributedString.DocumentType.html,
                                     .characterEncoding: String.Encoding.utf8.rawValue],
                            documentAttributes: nil)
                        self.aboutAndTermsTextView.attributedText = attrStr
                        
                    }
                }else{
                    let message = responseData["message"] as! String
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            //ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
    }
    
    //MARK:SOCIAL LOGIN URL SERVICE CALL
    func socialLoginUrlServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/social
            
            let postDict = [String : AnyObject]()
            ServicesHelper.sharedInstance.getDataServiceCall(url: socialAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of socialAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                //let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.socialLoginArray = [String]()
                    if let socialAccounts = responseData["data"] as? [[String : Any]]{
                        for item in socialAccounts{
                            let title = item["title"] as! String
                            if title == "facebook"{
                                self.facebookLink = item["url"] as? String
                            }else if title == "youtube"{
                                self.youtubeLink = item["url"] as? String
                            }else if title == "google"{
                                self.gmailLink = item["url"] as? String
                            }else if title == "instagram"{
                                self.instagramLink = item["url"] as? String
                            }else if title == "twitter"{
                                self.twitterLink = item["url"] as? String
                            }
                        }
                        print("facebookLink::\(self.facebookLink)")
                        print("youtubeLink::\(self.youtubeLink)")
                        print("gmailLink::\(self.gmailLink)")
                        print("instagramLink::\(self.instagramLink)")
                        print("twitterLink::\(self.twitterLink)")
                        if self.facebookLink != ""{
                            self.facebookBtn.isHidden = false
                        }else{
                            self.facebookBtn.isHidden = true
                        }
                        if self.youtubeLink != ""{
                            self.youtubeBtn.isHidden = false
                        }else{
                            self.youtubeBtn.isHidden = true
                        }
                        if self.gmailLink != ""{
                            self.gmailBtn.isHidden = false
                        }else{
                            self.gmailBtn.isHidden = true
                        }
                        if self.instagramLink != ""{
                            self.instaBtn.isHidden = false
                        }else{
                            self.instaBtn.isHidden = true
                        }
                        if self.twitterLink != ""{
                            self.twitterBtn.isHidden = false
                        }else{
                            self.twitterBtn.isHidden = true
                        }
                    }
                    
                }else{
                    let message = responseData["message"] as! String
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}
