//
//  SuccessVC.swift
//  GrowDoc
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class SuccessVC: UIViewController {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var thankyouLabel: UILabel!
    
    
    @IBOutlet weak var successfulPaymentLabel: UILabel!
    
    @IBOutlet weak var greatDayLabel: UILabel!
    
    @IBOutlet weak var checkMembershipBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.languageChangeString(a_str: "Payment success")
        self.thankyouLabel.text = self.languageChangeString(a_str: "Thank you!")
        self.successfulPaymentLabel.text = self.languageChangeString(a_str: "Successful payment.")
        self.greatDayLabel.text = self.languageChangeString(a_str: "Have a great day!")
        self.checkMembershipBtn.setTitle(self.languageChangeString(a_str: "Check my membership"), for: UIControl.State.normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "MyMembershipVC") as! MyMembershipVC
        blogVC.fromViewString = "success"
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    
    @IBAction func checkMembershipBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "MyMembershipVC") as! MyMembershipVC
        blogVC.fromViewString = "success"
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
