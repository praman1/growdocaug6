//
//  ScanVC.swift
//  GrowDoc
//
//  Created by volive solutions on 31/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import Alamofire
import CoreML
import Vision
import ImageIO


class ScanVC: UIViewController  {
    
    @IBOutlet weak var analysisLabel: UILabel!
    fileprivate var imgPicker: UIImagePickerController? = UIImagePickerController()
    var pickedImage  = UIImage()
    @IBOutlet var loadImageView: UIImageView!
    @IBOutlet var scanBtn: UIButton!
    var scanImage : UIImage?
    var postScanImage : UIImage?
    @IBOutlet var scanningImage: UIImageView!
    @IBOutlet var scanButtonWidthConstraint: NSLayoutConstraint!
    var userIdString : String! = ""
    
    var diseasesName = "";
    var languageString : String! = ""
    var membership_status : String! = ""
    var fromOptionString : String! = ""
    var flagValueString : String! = ""
    var genderString : String! = ""
    var healthString : String! = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.analysisLabel.text = self.languageChangeString(a_str: "Analysis of disease")
        self.scanningImage.image = self.postScanImage
//        self.scanBtn.contentHorizontalAlignment = .center
//        self.scanBtn.contentVerticalAlignment = .center
        DispatchQueue.main.async {
            self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
            self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
            self.loadImageView.isHidden = true
            self.scanButtonWidthConstraint.constant = 130
            
           // self.updateClassifications(for: self.postScanImage!)
        }
        
        //Group 397496
        // Do any additional setup after loading the view.
    }
    lazy var genderClassificationRequest : VNCoreMLRequest = {
        do {
            
          //  let model = try VNCoreMLModel(for:  GrowDocPlantsDiseases().model)

            
            let model = try VNCoreMLModel(for:  PlantGender().model)
            //  let model = try VNCoreMLModel(for:  CropDiseases1().model)
          //  let model = try VNCoreMLModel(for:  growdocImageClassifier().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                
                
                print("process classification",request)
                
                self?.processClassifications(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
    }()
    
    
    lazy var healthMeterClassificationRequest : VNCoreMLRequest = {
        do {
            
          //  let model = try VNCoreMLModel(for:  GrowDocPlantsDiseases().model)

            
            let model = try VNCoreMLModel(for:  PlantHealthMeter().model)
            //  let model = try VNCoreMLModel(for:  CropDiseases1().model)
          //  let model = try VNCoreMLModel(for:  growdocImageClassifier().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                
                
                print("process classification",request)
                
                self?.processClassifications(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
        
    }()
    
    
    // Tag: MLModelSetup
    lazy var classificationRequest: VNCoreMLRequest = {
       
        do {
            
         
            
          //  let model = try VNCoreMLModel(for:  GrowDocPlantsDiseases().model)

            
            let model = try VNCoreMLModel(for:  PlantDiseaseModelNew().model)
            //  let model = try VNCoreMLModel(for:  CropDiseases1().model)
          //  let model = try VNCoreMLModel(for:  growdocImageClassifier().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                
                
                print("process classification",request)
                
                self?.processClassifications(for: request, error: error)
            })
            request.imageCropAndScaleOption = .centerCrop
            return request
        } catch {
            fatalError("Failed to load Vision ML model: \(error)")
        }
    }()

    
    //MARK:- BACK BTN ACTION
    @IBAction func backBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func scanBtnAction(_ sender: Any) {
        self.scanBtn.setTitle(languageChangeString(a_str: "Scanning"), for: UIControl.State.normal)
        self.loadImageView.isHidden = false
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                self.scanButtonWidthConstraint.constant = 150
                print("ENGLISH_LANGUAGE")
             }else{
                self.scanButtonWidthConstraint.constant = 220
                print("FRENCH_LANGUAGE")
             }
        }else{
            self.scanButtonWidthConstraint.constant = 150
            print("ELSE ENGLISH_LANGUAGE")
        }
        self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 0)
        let imageData = try? Data(contentsOf: Bundle.main.url(forResource: "Eclipse-1s-200px (1)", withExtension: "gif")!)
        self.scanImage = UIImage.sd_animatedGIF(with: imageData)
        self.loadImageView.image = self.scanImage
       //self.relatedDataServiceCall()
        self.updateClassifications(for: self.postScanImage!)
      //  self.getDiseaseServiceCall()
    }
    
    
//func showAlert(message : String){
//        let alertController = UIAlertController(title: "Alert!", message: message, preferredStyle: .alert)
//
//        let saveAction = UIAlertAction(title: "OK", style: .default, handler: { alert -> Void in
//            let story = UIStoryboard.init(name: "Main", bundle: nil)
//            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//            self.present(homeVC, animated:true, completion: nil)
//        })
//        let cancelAction = UIAlertAction(title: "More Details", style: .default, handler: { (action : UIAlertAction!) -> Void in
//            let story = UIStoryboard.init(name: "Main", bundle: nil)
//            let galleryVC = story.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
//            self.navigationController?.pushViewController(galleryVC, animated: true)
//
//        })
//        alertController.view.tintColor = UIColor.init(red: 84.0/255.0, green: 144.0/255.0, blue: 0.0/255.0, alpha: 1.0)
//        alertController.addAction(saveAction)
//        alertController.addAction(cancelAction)
//        self.present(alertController, animated: true, completion: nil)
//    }

    //MARK:- GET DISEASES DETAILS SERVICE CALL
    
    func getDiseaseServiceCall() {
        
        if Reachability.isConnectedToNetwork() {
           // ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/related_data?lang=en&keyword=Leaf%20Miner&user_id=144
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["user_id":self.userIdString,"lang":self.languageString,"keyword":self.diseasesName]
            print("params", postDict)
            ServicesHelper.sharedInstance.getDataServiceCall(url: DISEASES_RELATED_DATA_API ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                
                print("Response of realted disease data", responseData)
                
                let status : Int = Int(responseData["status"] as! Int)
                //let message = responseData["message"] as! String
                if status == 1{
                //  ServicesHelper.sharedInstance.dissMissLoader()
                    DispatchQueue.main.async {
                        self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
                        self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
                        self.loadImageView.isHidden = true
                        self.scanButtonWidthConstraint.constant = 130
                    }
                    let message = responseData["message"] as! String
                    self.showToast(message: message)
                    //if let data = responseData?["Gallery"] as? [String : Any]{
                    //if responseData != nil{
                    let  story = UIStoryboard.init(name: "Main", bundle: nil)
                    let details = story.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
                    self.tabBarController?.hidesBottomBarWhenPushed = false
                    details.responseData = responseData
                    self.navigationController?.pushViewController(details, animated: true)
                    // }
                }
                else {
                    DispatchQueue.main.async {
                        self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
                        self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
                        self.loadImageView.isHidden = true
                        self.scanButtonWidthConstraint.constant = 130
                    }
                      // ServicesHelper.sharedInstance.dissMissLoader()
                    //let imageName = responseData["image"] as! String
                    let  story = UIStoryboard.init(name: "Main", bundle: nil)
                    let details = story.instantiateViewController(withIdentifier: "DeseaseNotDetectedViewController") as! DeseaseNotDetectedViewController
                    details.scannedImage = self.postScanImage ?? UIImage()
                  //  details.imageNameString = imageName
                    self.navigationController?.pushViewController(details, animated: true)
                }
            }
        }
        else {
         //   ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
    }
    
    //MARK: DETECT DISEASE SERVICE CALL
    func relatedDataServiceCall(){
        if Reachability.isConnectedToNetwork() {
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            //ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/related_data
            //user_id,lang,image
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            let postDict = ["user_id":self.userIdString,"lang":self.languageString,"keyword":self.diseasesName,"gender":self.genderString,"health":self.healthString,"flag":self.flagValueString]
            print("params", postDict)
            let imgData = self.postScanImage?.jpegData(compressionQuality: 0.2) ?? Data()
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(imgData, withName: "image",fileName: "image.jpg", mimeType: "image/jpg")
                for (key, value) in postDict {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
            },
                             to:DISEASES_RELATED_DATA_API)
            { (result) in
                print(result)
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                        print("Upload Progress: \(progress.fractionCompleted)")
                    })
                    upload.responseJSON { response in
                        print(response.result.value ?? "")
                        guard let responseData = response.result.value as? Dictionary<String, Any> else{
                            return
                        }
                        let status : Int = Int(responseData["status"] as! Int)
                        //let message = responseData["message"] as! String
                        self.membership_status = responseData["membership_status"] as? String
                        if self.membership_status == "1"{
                            UserDefaults.standard.set(true, forKey: "membership")
                        }else if self.membership_status == "0"{
                            UserDefaults.standard.set(false, forKey: "membership")
                        }
                        if status == 1{
                            //ServicesHelper.sharedInstance.dissMissLoader()
                            DispatchQueue.main.async {
                                self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
                                self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
                                self.loadImageView.isHidden = true
                                self.scanButtonWidthConstraint.constant = 130
                            }
                           let message = responseData["message"] as! String
                            self.showToast(message: message)
                            //if let data = responseData?["Gallery"] as? [String : Any]{
                            //if responseData != nil{
                            if self.membership_status == "0"{
                                let  story = UIStoryboard.init(name: "Main", bundle: nil)
                                let ads = story.instantiateViewController(withIdentifier: "AdsVC") as! AdsVC
                                self.tabBarController?.hidesBottomBarWhenPushed = false
                                ads.responseData = responseData
                                ads.status = "1"
                                ads.fromOptionString = self.fromOptionString
                                self.navigationController?.pushViewController(ads, animated: true)
                            }else{
                                let  story = UIStoryboard.init(name: "Main", bundle: nil)
                                let details = story.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
                                self.tabBarController?.hidesBottomBarWhenPushed = false
                                details.responseData = responseData
                                details.fromOptionString = self.fromOptionString
                                self.navigationController?.pushViewController(details, animated: true)
                            }

                           // }
                        }else {
                            DispatchQueue.main.async {
                                self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
                                self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
                                self.loadImageView.isHidden = true
                                self.scanButtonWidthConstraint.constant = 130
                            }
                         //   let imageName = responseData["image"] as! String
                            if self.membership_status == "0"{
                                let  story = UIStoryboard.init(name: "Main", bundle: nil)
                                let ads = story.instantiateViewController(withIdentifier: "AdsVC") as! AdsVC
                                self.tabBarController?.hidesBottomBarWhenPushed = false
                                //ads.responseData = responseData
                                ads.status = "0"
                                ads.scannedImage = self.postScanImage ?? UIImage()
                                ads.fromOptionString = self.fromOptionString
                                self.navigationController?.pushViewController(ads, animated: true)
                            }else{
                                 let  story = UIStoryboard.init(name: "Main", bundle: nil)
                                 let details = story.instantiateViewController(withIdentifier: "DeseaseNotDetectedViewController") as! DeseaseNotDetectedViewController
                                 //details.errorMessage = message
                                details.fromOptionString = self.fromOptionString
                                details.scannedImage = self.postScanImage ?? UIImage()
                                // details.imageNameString = imageName
                                 self.navigationController?.pushViewController(details, animated: true)
                            }
                        }
                    }
                case .failure(let encodingError):
                    print(encodingError)
                    ServicesHelper.sharedInstance.dissMissLoader()
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
}
/*
// MARK: - ImagePickerController Delegate and NavigationController Delegate
extension ScanVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        
        //picker.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
*/
//MARK:- RAMI SCANNER

// Tag: PerformRequests
extension  ScanVC{
    func updateClassifications(for image: UIImage) {
        let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
        guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation!)
            do {
                if self.fromOptionString == "gender"{
                    try handler.perform([self.genderClassificationRequest])
                    self.flagValueString = "0"
                    print("gender")
                }
                else if self.fromOptionString == "meter"{
                    try handler.perform([self.healthMeterClassificationRequest])
                    self.flagValueString = "0"
                    print("meter")
                }
                else{
                    try handler.perform([self.classificationRequest])
                    self.flagValueString = "1"
                    print("Disease")
                    self.fromOptionString = "Disease"
                }
            } catch {
                
                print("Failed to perform classification.\n\(error.localizedDescription)")
            }
        }
    }
    
    /// - Tag: ProcessClassifications
   
    func processClassifications(for request: VNRequest, error: Error?) {
        DispatchQueue.main.async {
            guard let results = request.results else {
                  self.diseasesName = ""
                return
            }
            let classifications = results as! [VNClassificationObservation]
            print("Scanned Classifications:::\(classifications)")
            if classifications.isEmpty {
            
                  self.diseasesName = ""
            }
            else {
                let topClassification = classifications.prefix(1)
                let descriptions = topClassification.map { classification -> String in
                    print("confidence disease",classification.confidence,classification.identifier)
                    //print("Gender of a plant",classification.)
                    print("String(classification.identifier):::",String(classification.identifier))
                    if (classification.confidence < 0.98) {
                        self.diseasesName = ""
                        self.genderString = ""
                        self.healthString = ""
                            //String(classification.identifier)
                           DispatchQueue.main.async {
                               self.scanBtn.contentEdgeInsets = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 0)
                               self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
                               self.loadImageView.isHidden = true
                               self.scanButtonWidthConstraint.constant = 130
                           }
                        //   let imageName = responseData["image"] as! String
                        //UserDefaults.standard.set(true, forKey: "membership")
                        if UserDefaults.standard.bool(forKey: "membership") == true{
                            self.membership_status = "1"
                        }else{
                            self.membership_status = "0"
                        }
                           if self.membership_status == "0"{
                               let  story = UIStoryboard.init(name: "Main", bundle: nil)
                               let ads = story.instantiateViewController(withIdentifier: "AdsVC") as! AdsVC
                               self.tabBarController?.hidesBottomBarWhenPushed = false
                               //ads.responseData = responseData
                               ads.status = "0"
                               ads.scannedImage = self.postScanImage ?? UIImage()
                               self.navigationController?.pushViewController(ads, animated: true)
                           }else{
                                let  story = UIStoryboard.init(name: "Main", bundle: nil)
                                let details = story.instantiateViewController(withIdentifier: "DeseaseNotDetectedViewController") as! DeseaseNotDetectedViewController
                                //details.errorMessage = message
                               details.fromOptionString = self.fromOptionString
                                   details.scannedImage = self.postScanImage ?? UIImage()
                               // details.imageNameString = imageName
                                self.navigationController?.pushViewController(details, animated: true)
                           }
                        return ""
                            //String(classification.identifier)
                    }
                    else{
                        if self.fromOptionString == "gender"{
                           self.genderString = String(classification.identifier)
                            print("self.genderString:::\(self.genderString ?? "")")
                        }
                        else if self.fromOptionString == "meter"{
                            //1.Healthy, 2.Minor, 3.Major,4. Severe, 5.Extreme
                           self.healthString = String(classification.identifier)
                            if self.healthString == "1(Dead)"{
                                self.healthString = "1"
                            }else if self.healthString == "5(Healthy)"{
                                self.healthString = "5"
                            }else{
                                self.healthString = String(classification.identifier)
                            }
                            print("self.healthString:::\(self.healthString ?? "")")
                        }
                        else{
                            self.diseasesName = String(classification.identifier)
                            print("self.diseasesName:::\(self.diseasesName)")
                        }
                        self.relatedDataServiceCall()
                        return String(classification.identifier)
                        
                    }
                }
                print("diseases name",descriptions[0])
                print("descriptions:::",descriptions)
                //print("Response Disease:::",self.diseasesName)
            }
        }
    }
}

extension CGImagePropertyOrientation {
    /**
     Converts a `UIImageOrientation` to a corresponding
     `CGImagePropertyOrientation`. The cases for each
     orientation are represented by different raw values.
     
     - Tag: ConvertOrientation
     */
    init(_ orientation: UIImage.Orientation) {
        switch orientation {
        case .up: self = .up
        case .upMirrored: self = .upMirrored
        case .down: self = .down
        case .downMirrored: self = .downMirrored
        case .left: self = .left
        case .leftMirrored: self = .leftMirrored
        case .right: self = .right
        case .rightMirrored: self = .rightMirrored
        }
    }
}
/*
[<VNClassificationObservation: 0x28313f9c0> 04DF1719-5CF1-46AC-AAF2-7BCC03EB9AAA, revision 1, 0.407259 "3", <VNClassificationObservation: 0x28313f690> D6485AEA-4D15-43E7-A4E7-BBA27EF783EB, revision 1, 0.276110 "1(Dead)", <VNClassificationObservation: 0x28313cd50> 36CD1A49-C233-4E79-A24D-45FF02FC867C, revision 1, 0.110815 "2", <VNClassificationObservation: 0x28313cea0> 44965F5D-D2D0-491E-BE3F-EEFD3B13B841, revision 1, 0.106386 "4", <VNClassificationObservation: 0x28313fba0> 7368855C-427E-4030-B15E-63AF2B95B96C, revision 1, 0.099430 "5(Healthy)"]
 */
