//
//  PhotoVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class PhotoVC: UIViewController {

    var pickedImage : UIImage?
    var checkPush : String! = ""
    @IBOutlet var selectedImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedImageView.image = pickedImage
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    //MARK:- BACK BTN ACTION
    @IBAction func backBtnAction(_ sender: Any) {
//        if self.checkPush == "Home"{
//            self.navigationController?.popViewController(animated: true)
//        }else{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
            homeVC.modalPresentationStyle = .fullScreen
            self.present(homeVC, animated:true, completion: nil)
        //}
    }
    
    @IBAction func nextBtnAction(_ sender: Any) {
        let  story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        self.navigationController?.pushViewController(scanVC, animated: true)
    }
}
