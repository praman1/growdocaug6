//
//  ShowImageViewController.swift
//  GrowDoc
//
//  Created by volivesolutions on 29/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class ShowImageViewController: UIViewController,UIScrollViewDelegate {

   // @IBOutlet var cropImageView: UIImageView!
    
    @IBOutlet var similarImagesCollectionView: UICollectionView!
    var imageArray = [String]()
    //@IBOutlet var imageZoomScrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.object(forKey: "cropImage") != nil{
            self.imageArray = UserDefaults.standard.object(forKey: "cropImage") as! [String]
            self.similarImagesCollectionView.reloadData()
        }
        
        //cropImage
//        self.imageZoomScrollView.minimumZoomScale = 1.0
//        self.imageZoomScrollView.maximumZoomScale = 6.0
//        self.cropImageView.sd_setImage(with: URL (string: UserDefaults.standard.object(forKey: "cropImage") as! String), placeholderImage: UIImage(named:""))
        // Do any additional setup after loading the view.
    }

    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ShowImageViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = self.similarImagesCollectionView.dequeueReusableCell(withReuseIdentifier: "SimilarImagesCollectionViewCell", for: indexPath) as! SimilarImagesCollectionViewCell
            cell.cropImageView.contentMode = .scaleAspectFit
            cell.cropImageView.setKFImage(with: self.imageArray[indexPath.row])
           // cell.cropImageView.sd_setImage(with: URL (string: self.imageArray[indexPath.row]), placeholderImage:UIImage(named: ""))
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.similarImagesCollectionView.frame.size.width, height: self.similarImagesCollectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}
