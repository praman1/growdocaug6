//
//  GalleryVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class GalleryVC: UIViewController {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    var fromSideMenu:String!
    var flagString : String! = ""
    var languageString : String! = ""
    var fromOptionString : String! = ""
    var galleryArray = [GalleryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.fromOptionString == "gender"{
            self.flagString = "1"
            self.headerLabel.text = self.languageChangeString(a_str: "Plant Male/Female")
            self.titleLabel.text = self.languageChangeString(a_str: "Plant Male/Female")
        }else if self.fromOptionString == "meter"{
            self.flagString = "2"
            self.headerLabel.text = self.languageChangeString(a_str: "Plant Health Meter")
            self.titleLabel.text = self.languageChangeString(a_str: "Plants Health Meter")
        }else{
            self.flagString = "0"
            self.headerLabel.text = self.languageChangeString(a_str: "Plant Problems")
            self.titleLabel.text = self.languageChangeString(a_str: "Plants Problems")
        }
        self.galleryServiceCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) { 
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        if fromSideMenu == "Yes"{
             present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
             self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: GALLRY SERVICE CALL
    func galleryServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/gallery?lang=
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["lang":self.languageString,"flag":self.flagString]
            print("galleryAPI Post Dict:::\(postDict)")
            ServicesHelper.sharedInstance.getDataServiceCall(url: galleryAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of galleryAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                //let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.galleryArray = [GalleryModel]()
                    if let gallery = responseData["data"] as? [[String : Any]]{
                        for item in gallery{
                            let galleryObject = GalleryModel(galleryData: item as NSDictionary)
                            self.galleryArray.append(galleryObject)
                        }
                    }
                    DispatchQueue.main.async {
                        self.galleryCollectionView.reloadData()
                    }
                }else{
                    let message = responseData["message"] as! String
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}

extension GalleryVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.galleryArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = galleryCollectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        let galleryObject = self.galleryArray[indexPath.row]
        let imageString : String = BASE_PATH + galleryObject.image
        cell.plantsImageView.setKFImage(with: imageString)
        //cell.plantsImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
        cell.plantsName_lbl.text = galleryObject.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = galleryCollectionView.bounds.width/2.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         let story = UIStoryboard.init(name: "Main", bundle: nil)
        let galleryDetailsVC = story.instantiateViewController(withIdentifier: "GalleryDetailsVC") as! GalleryDetailsVC
//            galleryDetailsVC.selectedImage = plantsImagesArray[indexPath.row]
//            galleryDetailsVC.selectedPlantName = plantsNames[indexPath.row]
            galleryDetailsVC.galleryIdString = self.galleryArray[indexPath.row].galleryId
        self.navigationController?.pushViewController(galleryDetailsVC, animated: true)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}
