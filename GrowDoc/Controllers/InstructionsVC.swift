//
//  InstructionsVC.swift
//  GrowDoc
//
//  Created by Apple on 29/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit
import AVKit
class InstructionsVC: UIViewController {
    fileprivate var imgPicker: UIImagePickerController? = UIImagePickerController()
    var pickedImage  = UIImage()
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var donotShowLabel: UILabel!
    @IBOutlet weak var doNotShowCheckBtn: UIButton!
    
    @IBOutlet weak var examplesOfLabel: UILabel!
    @IBOutlet weak var instructionsInfoLabel: UILabel!
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
    
    @IBOutlet weak var videoHeight: NSLayoutConstraint!
    @IBOutlet weak var scanHeaderLabel: UILabel!
    
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var scanBtn: UIButton!
    @IBOutlet weak var startScanBtn: UIButton!
    
    @IBOutlet weak var instructionImageView: UIImageView!
    
    @IBOutlet weak var videoImageView: UIImageView!
    var flagString : String! = ""
    var fromOptionString : String! = ""
    var fromViewString : String! = ""
    var languageString : String! = ""
    var videoUrl: URL?
    var textString : String?
    var testString = "Go for Premium Membership for  immediate and accurate results Instructions camera InstructionsVC Membership"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backBtn.layer.borderColor = UIColor.white.cgColor
        self.backBtn.layer.borderWidth = 1.0
        self.backBtn.layer.cornerRadius = 5
        
        self.scanBtn.layer.borderColor = UIColor.white.cgColor
        self.scanBtn.layer.borderWidth = 1.0
        self.scanBtn.layer.cornerRadius = 5
        
        self.scanHeaderLabel.text = self.languageChangeString(a_str: "Scan Instructions")
        self.examplesOfLabel.text = self.languageChangeString(a_str: "Examples of leaf scanning")
        self.titleLabel.text = self.languageChangeString(a_str: "Instructions")
        
        if self.fromOptionString == "gender"{
            self.flagString = "1"
           // self.doNotShowCheckBtn.isHidden = false
        }else if self.fromOptionString == "meter"{
            self.flagString = "2"
            
           // self.doNotShowCheckBtn.isHidden = false
        }else{
            self.flagString = "0"
           
            //self.doNotShowCheckBtn.isHidden = false
        }
        self.donotShowLabel.text = self.languageChangeString(a_str: "Don't show this again")
        self.backBtn.setTitle(self.languageChangeString(a_str: "Back"), for: UIControl.State.normal)
        self.scanBtn.setTitle(self.languageChangeString(a_str: "Scan"), for: UIControl.State.normal)
        self.startScanBtn.setTitle(self.languageChangeString(a_str: "Start Scanning Now"), for: UIControl.State.normal)
        self.instructionsServiceCall()
        self.addGesture()
       // self.instructionsInfoLabel.text = self.testString
       // self.imageHeight.constant = 200
        //self.videoHeight.constant = 200
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        if self.fromOptionString == "gender" || self.fromOptionString == "meter"{
            self.navigationController?.popViewController(animated: true)
        }else{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
            homeVC.modalPresentationStyle = .fullScreen
            self.present(homeVC, animated:true, completion: nil)
        }
    }
    
    @IBAction func doNotShowCheckBtnAction(_ sender: Any) {
        if self.doNotShowCheckBtn.tag == 0{
            self.doNotShowCheckBtn.tag = 1
            self.doNotShowCheckBtn.setImage(UIImage.init(named: "Group 3974"), for: UIControl.State.normal)
            if self.flagString == "1"{
                UserDefaults.standard.set(true, forKey: "doNotshowgender")
            }else if self.flagString == "2"{
                UserDefaults.standard.set(true, forKey: "doNotshowmeter")
            }else{
                UserDefaults.standard.set(true, forKey: "doNotshowscan")
            }
        }else{
            self.doNotShowCheckBtn.tag = 0
            self.doNotShowCheckBtn.setImage(UIImage.init(named: "Rectangle 3549"), for: UIControl.State.normal)
            if self.flagString == "1"{
                UserDefaults.standard.set(false, forKey: "doNotshowgender")
            }else if self.flagString == "2"{
                UserDefaults.standard.set(false, forKey: "doNotshowmeter")
            }else{
                UserDefaults.standard.set(false, forKey: "doNotshowscan")
            }
        }
    }
    
    @IBAction func startScanBtnAction(_ sender: Any) {
        if self.fromViewString == "camera"{
            self.imgPicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
        }else{
            //self.dismiss(animated: true, completion: nil)
            self.imgPicker?.delegate = self
            if let imagePicker = self.imgPicker {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        if self.fromViewString == "camera"{
            self.imgPicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
        }else{
            //self.dismiss(animated: true, completion: nil)
            self.imgPicker?.delegate = self
            if let imagePicker = self.imgPicker {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func camera(){
        self.imgPicker?.delegate = self
        self.imgPicker?.allowsEditing = true
        self.imgPicker?.sourceType = UIImagePickerController.SourceType.camera
        self.imgPicker?.cameraCaptureMode = .photo
        self.imgPicker?.modalPresentationStyle = .fullScreen
        self.imgPicker?.cameraFlashMode = UIImagePickerController.CameraFlashMode.off
        self.present(self.imgPicker!,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: self.languageChangeString(a_str: "No Camera"),message: self.languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: self.languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    //MARK:instructionsServiceCall
    func instructionsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/scan_instructions?lang=
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            let postDict = ["lang":self.languageString,"flag":self.flagString]
            print("scanInstructions Post Dict:::\(postDict)")
            ServicesHelper.sharedInstance.getDataServiceCall(url: scanInstructions ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    if let data = responseData["data"] as? [String: Any] {
                        //let termsStr = termsData.text
//                        DispatchQueue.main.async {
//                            self.aboutAndTermsTextView.font = UIFont(name: "Poppins-Regular", size: 16.0)
//                            self.aboutAndTermsTextView.textColor = UIColor.init(red: 115.0/255.0, green: 115.0/255.0, blue: 115.0/255.0, alpha: 1.0)
//                        }
                        self.textString = data["scan_instructions"] as? String
                        let attrStr = try! NSAttributedString(
                            data: (self.textString?.data(using: .unicode, allowLossyConversion: true)!)!,
                            options:[.documentType: NSAttributedString.DocumentType.html,
                                     .characterEncoding: String.Encoding.utf8.rawValue],
                            documentAttributes: nil)
                        self.instructionsInfoLabel.text = attrStr.string
                        if data["image"] as? String != ""{
                            let image = data["image"] as? String
                            let imageString : String = BASE_PATH + image!
                            self.instructionImageView.setKFImage(with: imageString)
                            //self.instructionImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                            self.imageHeight.constant  = 200
                        }else{
                            self.imageHeight.constant  = 0
                        }
                        
                        if data["video"] as? String != ""{
                            let videoString = data["video"] as! String
                            self.videoUrl = URL(string: BASE_PATH + videoString)
                            let image = data["thumbnail"] as? String
                            let imageString : String = BASE_PATH + image!
                            self.videoImageView.setKFImage(with: imageString)
                            //self.videoImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                            self.videoHeight.constant  = 200
                        }else{
                            self.videoHeight.constant  = 0
                        }
                    }
                }else{
                    let message = responseData["message"] as! String
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
        }
    }
    
    //MARK:addGesture
    func addGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tappedImage))
        self.videoImageView.addGestureRecognizer(tapGesture)
    }
    
    @objc func tappedImage(){
        if self.videoUrl != nil{
            let player = AVPlayer(url: videoUrl!)
            let vc = AVPlayerViewController()
            vc.player = player
            present(vc, animated: true) {
                vc.player?.play()
            }
        }
    }
}

// MARK: - ImagePickerController Delegate and NavigationController Delegate
extension InstructionsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
        }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        
        //picker.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        scanVC.fromOptionString = self.fromOptionString
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
