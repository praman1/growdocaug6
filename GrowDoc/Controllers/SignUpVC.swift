//
//  SignUpVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController {

    @IBOutlet weak var iAgreeLabel: UILabel!
    
    @IBOutlet weak var termsBtn: UIButton!
    
    @IBOutlet weak var signUpBtn: UIButton!
    
    @IBOutlet weak var alreadyHaveAccountLabel: UILabel!
    
    @IBOutlet weak var signInNowBtn: UIButton!
    
    @IBOutlet weak var continueAsLabel: UILabel!
    
    @IBOutlet weak var guestBtn: UIButton!
    
    @IBOutlet weak var alreadyHaveWidthConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var emailAddressTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var confirmPasswordTextField: UITextField!
    @IBOutlet var checkBoxBtn: UIButton!
    @IBOutlet var passwordShowHideBtn: UIButton!
    @IBOutlet var confirmPasswordShowHideBtn: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    var emailIsString : String?
    var languageString : String! = ""
    var guestCheck : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkBoxBtn.tag = 2
        self.passwordShowHideBtn.tag = 1
        self.confirmPasswordShowHideBtn.tag = 1
        self.passwordTextField.isSecureTextEntry = true
        self.confirmPasswordTextField.isSecureTextEntry = true
        self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
        self.confirmPasswordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
        self.tabBarController?.tabBar.isHidden = true
        self.nameTextField.placeholder = self.languageChangeString(a_str:"Username")
        self.emailAddressTextField.placeholder = self.languageChangeString(a_str: "E-mail Address*")
        self.passwordTextField.placeholder = self.languageChangeString(a_str: "Password*")
        self.confirmPasswordTextField.placeholder = self.languageChangeString(a_str: "Confirm password*")
        self.iAgreeLabel.text = self.languageChangeString(a_str: "I agree to the")
        self.termsBtn.setTitle(self.languageChangeString(a_str: "Terms and Conditions"), for: UIControl.State.normal)
        self.signUpBtn.setTitle(self.languageChangeString(a_str: "SIGN UP"), for: UIControl.State.normal)
        self.alreadyHaveAccountLabel.text = self.languageChangeString(a_str: "Already have an account?")
        self.signInNowBtn.setTitle(self.languageChangeString(a_str: "Sign in Now!"), for: UIControl.State.normal)
        self.orLabel.text = self.languageChangeString(a_str: "OR")
        self.continueAsLabel.text = self.languageChangeString(a_str: "Continue as")
        self.guestBtn.setTitle(self.languageChangeString(a_str: "Stay Anonymous"), for: UIControl.State.normal)
        self.titleLabel.text = self.languageChangeString(a_str: "Sign up")
        
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                alreadyHaveWidthConstraint.constant = 200
                print("ENGLISH_LANGUAGE")
             }else{
                alreadyHaveWidthConstraint.constant = 130
                print("FRENCH_LANGUAGE")
             }
        }else{
            alreadyHaveWidthConstraint.constant = 200
            print("ELSE ENGLISH_LANGUAGE")
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func termsAndConditionBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "AboutVCViewController") as! AboutVCViewController
          blogVC.fromVC = "SignUP"
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func signUpBtnAction(_ sender: Any) {
            if self.nameTextField.text == "" {
                showToastForAlert(message: self.languageChangeString(a_str: "Please enter full name")!)
            }else if self.emailAddressTextField.text == ""{
                showToastForAlert(message: self.languageChangeString(a_str: "Please enter email address")!)
            }else if self.passwordTextField.text == ""{
                showToastForAlert(message: self.languageChangeString(a_str: "Please enter password")!)
            }else if self.confirmPasswordTextField.text == ""{
                showToastForAlert(message: self.languageChangeString(a_str: "Please enter confirm password")!)
            }else if (self.passwordTextField.text != self.confirmPasswordTextField.text){
                showToastForAlert(message: self.languageChangeString(a_str: "Password and Confirm Password doesn't match")!)
            }else if emailAddressTextField.text != ""{
                do {
                    let email = try self.emailAddressTextField.validatedText(validationType: ValidatorType.email)
                    self.emailIsString = email
                    self.registrationServiceCall()
                } catch(let error) {
                    //showToast(message: (error as! ValidationError).message)
                    showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
                }
            }
    }
    
    @IBAction func guestBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        // homeVC.selectedIndex = 0
        UserDefaults.standard.set(true, forKey: "isGuest")
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
    }
    
    @IBAction func signInBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let signInVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        signInVC.guestCheckString = self.guestCheck
        self.navigationController?.pushViewController(signInVC, animated: true)
    }
    
    @IBAction func passwordShowHideBtnAction(_ sender: Any) {
        if self.passwordShowHideBtn.tag == 1 {
            //SHOW
            self.passwordTextField.isSecureTextEntry = false
            self.passwordShowHideBtn.tag = 2
            self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 2347"), for: UIControl.State.normal)
        }
        else{
            //HIDE
            self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
            self.passwordTextField.isSecureTextEntry = true
            self.passwordShowHideBtn.tag = 1
        }
    }
    
    @IBAction func confirmPasswordShowHideBtnAction(_ sender: Any) {
        if self.confirmPasswordShowHideBtn.tag == 1 {
            //SHOW
            self.confirmPasswordTextField.isSecureTextEntry = false
            self.confirmPasswordShowHideBtn.tag = 2
            self.confirmPasswordShowHideBtn.setImage(UIImage.init(named: "Group 2347"), for: UIControl.State.normal)
        }
        else{
            //HIDE
            self.confirmPasswordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
            self.confirmPasswordTextField.isSecureTextEntry = true
            self.confirmPasswordShowHideBtn.tag = 1
        }
    }
    
    @IBAction func checkBoxBtnAction(_ sender: Any) {
        if self.checkBoxBtn.tag == 1 {
            // UNCheck
            self.checkBoxBtn.tag = 2
            self.checkBoxBtn.setImage(UIImage.init(named: "Rectangle 3549"), for: UIControl.State.normal)
        }
        else{
            // Check
            self.checkBoxBtn.setImage(UIImage.init(named: "Group 3974"), for: UIControl.State.normal)
            self.checkBoxBtn.tag = 1
        }
    }
    
    //MARK:REGISTRATION SERVICE CALL
    func registrationServiceCall(){
        if self.checkBoxBtn.tag == 1{
            if Reachability.isConnectedToNetwork() {
                ServicesHelper.sharedInstance.loader(view: self.view)
                //https://www.demomaplebrains.com/growdoc_dev/services/registration?device_name=__&device_token=__&username=__&email=__&password=__
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
                let postDict = ["device_name":DEVICE_TYPE,"device_token":DEVICE_TOKEN,"username":self.nameTextField.text!,"email":self.emailAddressTextField.text!,"password":self.passwordTextField.text!,"lang":self.languageString]
                ServicesHelper.sharedInstance.getDataServiceCall(url: registrationAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                    print("Response of registrationAPI ",responseData)
                    let status : Int = Int(responseData["status"] as! Int)
                    let message = responseData["message"] as! String
                    if status == 1{
                        ServicesHelper.sharedInstance.dissMissLoader()
                         self.showToast(message: message)
                        //self.navigationController?.popViewController(animated: true)
                        
                        let story = UIStoryboard.init(name: "Main", bundle: nil)
                        let signInVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
                        signInVC.guestCheckString = self.guestCheck
                        self.navigationController?.pushViewController(signInVC, animated: true)
                        //self.navigationController?.popToViewController(signInVC, animated: true)
                        
                    }else{
                        ServicesHelper.sharedInstance.dissMissLoader()
                        self.showToastForAlert(message: message)
                    }
                }
            }else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
                //self.showToastForAlert(message: "Please check your Internet Connection")
            }
        }else{
            showToastForAlert(message: languageChangeString(a_str: "Please accept terms & conditions")!)
        }
    }
    
}
