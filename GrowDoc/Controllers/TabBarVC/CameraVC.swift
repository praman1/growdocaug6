//
//  CameraVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
class CameraVC: UIViewController {

    var imagePicker = UIImagePickerController()
    var pickedImage  = UIImage()
   // private var flashMode: AVCaptureDevice.FlashMode = .auto
    var checkPush : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        //UserDefaults.standard.set(false, forKey: "membership")
        if UserDefaults.standard.bool(forKey: "membership") == false{
            self.popUp()
        }else{
            if UserDefaults.standard.bool(forKey: "doNotshowscan") == true{
                self.imagePicker = UIImagePickerController()
                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                    self.camera()
                } else {
                    self.noCamera()
                }
            }else{
                 let story = UIStoryboard.init(name: "Main", bundle: nil)
                 let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                   //blogVC.fromVC = "SignUP"
                 instructionsVC.fromViewString = "camera"
                // instructionsVC.fromOptionString = self.fromOptionString
                 self.navigationController?.pushViewController(instructionsVC, animated: true)
            }

//            self.imagePicker = UIImagePickerController()
//            if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                self.camera()
////                DispatchQueue.main.async {
////                    self.imagePicker.cameraFlashMode = .on
////                }
//            } else {
//                self.noCamera()
//            }
        }
            //view.dismiss(animated: true)
        
        // Do any additional setup after loading the view.
    }
    
    func popUp(){
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PopUpVC = story.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
        var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
        let navigation = UINavigationController.init(rootViewController: PopUpVC )
        PopUpVC.fromViewString = "camera"
        navigation.modalPresentationStyle = .overCurrentContext
        while ((viewcontroller?.presentedViewController) != nil)
        {
            viewcontroller = viewcontroller?.presentedViewController
        }
        viewcontroller?.present(navigation, animated: true, completion: nil)
    }
    
    func camera(){
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
        self.imagePicker.cameraCaptureMode = .photo
        self.imagePicker.modalPresentationStyle = .fullScreen
        self.imagePicker.cameraFlashMode = UIImagePickerController.CameraFlashMode.off
        self.present(self.imagePicker,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: self.languageChangeString(a_str: "No Camera"),message: self.languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: self.languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
//    private func getSettings(camera: AVCaptureDevice, flashMode: AVCaptureDevice.FlashMode) -> AVCapturePhotoSettings {
//        let settings = AVCapturePhotoSettings()
//
//        if camera.hasFlash {
//            settings.flashMode = flashMode
//        }
//        return settings
//    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.imagePicker = UIImagePickerController()
//        self.imagePicker.delegate = self
//
//        if UIImagePickerController.isSourceTypeAvailable(.camera) {
//            self.camera()
//        } else {
//            self.noCamera()
//        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
       // dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanBtnAction(_ sender: Any) {
        let  story = UIStoryboard.init(name: "Main", bundle: nil)
        let photoVC = story.instantiateViewController(withIdentifier: "PhotoVC") as! PhotoVC
        tabBarController?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(photoVC, animated: true)
    }
}  

//@available(iOS 10.0, *)
extension CameraVC: UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        DispatchQueue.main.async {
            if self.checkPush == "Home"{
                self.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
                homeVC.modalPresentationStyle = .fullScreen
                self.present(homeVC, animated:true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        scanVC.fromOptionString = ""
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
