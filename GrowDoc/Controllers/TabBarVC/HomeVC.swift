//
//  HomeVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu
import CoreML
import Vision
import ImageIO
class HomeVC: UIViewController {
    
    @IBOutlet weak var titleLblTopConstarint: NSLayoutConstraint!
    @IBOutlet weak var viewHightConstraint: NSLayoutConstraint!
    fileprivate var imgPicker: UIImagePickerController? = UIImagePickerController()
    
    @IBOutlet weak var healthHintLabel: UILabel!
    @IBOutlet weak var genderHintLabel: UILabel!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var maleFemaleBtn: UIButton!
    @IBOutlet weak var plantMeterBtn: UIButton!
    
    @IBOutlet weak var blogBtn: UIButton!
    
    @IBOutlet weak var viewPlantProblemsBtn: UIButton!
    
    @IBOutlet weak var title_lbl: UILabel!
    
    @IBOutlet var loginBtn: UIButton!
    
    @IBOutlet weak var subTitle_lbl: UILabel!
    
    @IBOutlet weak var meterHeaderLabel: UILabel!
    @IBOutlet weak var homeImageView: UIImageView!
    @IBOutlet var historyBtn: UIButton!
    
    @IBOutlet weak var discoverHeaderLabel: UILabel!
    
    @IBOutlet weak var discoverLabel: UILabel!
    @IBOutlet weak var imageTopConstarint: NSLayoutConstraint!
    var pickedImage  = UIImage()
    var bannerArray = [BannerModel]()
    var userIdString : String! = ""
    var membership_status : String! = ""
    var fromOptionString : String! = ""
    var remainedScans : Int!
    var languageString : String! = ""
   // var tabBar = UITabBar()
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        guard let items = tabBar.items else { return }
//        items[0].title = self.languageChangeString(a_str: "Home")
//        items[2].title = self.languageChangeString(a_str: "Browse Picture")
        self.welcomeLabel.text = self.languageChangeString(a_str: "Welcome to GrowDoc")
        self.maleFemaleBtn.setTitle(self.languageChangeString(a_str: "Male/Female Scanner"), for: UIControl.State.normal)
        self.genderHintLabel.text = self.languageChangeString(a_str: "Scan to identify sexe of plant")
       // self.plantMeterBtn.setTitle(self.languageChangeString(a_str: "Plant Health Meter"), for: UIControl.State.normal)
        self.meterHeaderLabel.text = self.languageChangeString(a_str: "Plant Health Meter")
        self.healthHintLabel.text = self.languageChangeString(a_str: "Scan to see the severity of your plant problem")
        self.loginBtn.setTitle(self.languageChangeString(a_str: "Login"), for: UIControl.State.normal)
        self.subTitle_lbl.text = self.languageChangeString(a_str: "Take a picture of your crop to detect diseases and find out solutions and recommendations")
        self.blogBtn.setTitle(self.languageChangeString(a_str: "Information about growing cannabis(Blog)"), for: UIControl.State.normal)
        self.viewPlantProblemsBtn.setTitle(self.languageChangeString(a_str: "View Plant Problems"), for: UIControl.State.normal)
        self.historyBtn.setTitle(self.languageChangeString(a_str: "View Scan History"), for: UIControl.State.normal)
        self.discoverHeaderLabel.text = self.languageChangeString(a_str: "Discover your Plant Solutions")
        self.discoverLabel.text = self.languageChangeString(a_str: "Identify plant diseases,pests and nutrient deficiencies.Empowers to control them and increase your yield")
       //  addGesture()
        let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuVC)
        menuLeftNavigationController.leftSide = true
 
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1
        
        iPhoneScreenSizes()
        self.navigationController?.isNavigationBarHidden = true
        self.membershipCheckServiceCall()
        if UserDefaults.standard.bool(forKey: "InfoFirst") != true{
            self.membershipInfoServiceCall()
            
        }
    }

    func membershipInfoServiceCall(){
            if Reachability.isConnectedToNetwork() {
                //ServicesHelper.sharedInstance.loader(view: self.view)
                //http://demomaplebrains.com/growdoc_phase/services/membership_popup?lang=
                if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                    self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
                }
                let postDict = ["lang" : self.languageString]
                ServicesHelper.sharedInstance.getDataServiceCall(url: membership_popup ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                    let status : Int = Int(responseData["status"] as! Int)
                    if status == 1{
                        //ServicesHelper.sharedInstance.dissMissLoader()
                        if let data = responseData["data"] as? [String: Any] {
                            if data["text"] as? String != ""{
                                let text = data["text"] as? String
                               UserDefaults.standard.set(true, forKey: "InfoFirst")
                               let story = UIStoryboard.init(name: "Main", bundle: nil)
                               let PopUpVC = story.instantiateViewController(withIdentifier: "InformationPopUpVC") as! InformationPopUpVC
                               var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
                                PopUpVC.infoTextString = text
                               let navigation = UINavigationController.init(rootViewController: PopUpVC )
                               navigation.modalPresentationStyle = .overCurrentContext
                               while ((viewcontroller?.presentedViewController) != nil)
                               {
                                   viewcontroller = viewcontroller?.presentedViewController
                               }
                               viewcontroller?.present(navigation, animated: true, completion: nil)
                            }else{
                                //self.imageHeight.constant  = 0
                            }
                        }
                    }else{
                        let message = responseData["message"] as! String
                        //ServicesHelper.sharedInstance.dissMissLoader()
                        self.showToastForAlert(message: message)
                    }
                }
            }else {
                //ServicesHelper.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            }
        }
    // MARK: Add Gesture Fucntion
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapedImageView))
        self.homeImageView.isUserInteractionEnabled = true
        homeImageView.addGestureRecognizer(tapGesture)
    }

    // MARK: Taped ImageView Notification
    @objc func tapedImageView() {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let cameraVC = story.instantiateViewController(withIdentifier: "CameraVC") as! CameraVC
        tabBarController?.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(cameraVC, animated: true)
    }
    
    func popUp(){
        UserDefaults.standard.set(true, forKey: "InfoFirst")
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PopUpVC = story.instantiateViewController(withIdentifier: "InformationPopUpVC") as! InformationPopUpVC
        var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
        let navigation = UINavigationController.init(rootViewController: PopUpVC )
        navigation.modalPresentationStyle = .overCurrentContext
        while ((viewcontroller?.presentedViewController) != nil)
        {
            viewcontroller = viewcontroller?.presentedViewController
        }
        viewcontroller?.present(navigation, animated: true, completion: nil)
    }
    
    func iPhoneScreenSizes(){
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        print("height ",height)
        switch height {
        case 480.0:
            print("iPhone 3,4")
        case 568.0:
            viewHightConstraint.constant = +50
            titleLblTopConstarint.constant = 50
            title_lbl.font = AppFont.semiBold(size: 19)
            subTitle_lbl.font = AppFont.regular(size: 13)
            print("iPhone 5")
        case 667.0:
            print("iPhone 6")
            if UserDefaults.standard.object(forKey: "userId") != nil{
                print("iPhone 6 LOGIN")
                viewHightConstraint.constant = +70
                imageTopConstarint.constant = -80
                titleLblTopConstarint.constant = 70
            }else{
                print("iPhone 6 ELSE LOGIN")
                viewHightConstraint.constant = +50
                imageTopConstarint.constant = -90
                titleLblTopConstarint.constant = 50
            }
        case 736.0:
            print("iPhone 6+")
            title_lbl.font = AppFont.semiBold(size: 21)
            subTitle_lbl.font = AppFont.regular(size: 15)
            imageTopConstarint.constant = -90
        case 896.0:
            title_lbl.font = AppFont.semiBold(size: 23)
            subTitle_lbl.font = AppFont.regular(size: 17)
            imageTopConstarint.constant = -110
            print("iPhone x+")
        default:
            print("not an iPhone")
            
        }
    }
    
    func popUpView(){
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PopUpVC = story.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
        var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
        let navigation = UINavigationController.init(rootViewController: PopUpVC )
        PopUpVC.fromViewString = "camera"
        PopUpVC.fromOptionString = self.fromOptionString
        navigation.modalPresentationStyle = .overCurrentContext
        while ((viewcontroller?.presentedViewController) != nil)
        {
            viewcontroller = viewcontroller?.presentedViewController
        }
        viewcontroller?.present(navigation, animated: true, completion: nil)
    }
    
    //MARK:maleFemaleBtnAction
    @IBAction func maleFemaleBtnAction(_ sender: Any) {
        
        self.fromOptionString = "gender"
                if self.membership_status == "1"{
                    if self.remainedScans >= 1{
                        let view = UIAlertController(title: self.languageChangeString(a_str: ""), message: self.languageChangeString(a_str: "Take a picture or Choose a picture from library"), preferredStyle: .actionSheet)
                        let PhotoLibrary = UIAlertAction(title: self.languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
                           // self.PhotoLibrary()
                            //self.PhotoLibrary()
                            if UserDefaults.standard.bool(forKey: "doNotshowgender") == true{
                                self.PhotoLibrary()
                            }else{
                                let story = UIStoryboard.init(name: "Main", bundle: nil)
                                let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                                  //blogVC.fromVC = "SignUP"
                                instructionsVC.fromViewString = ""
                                instructionsVC.fromOptionString = self.fromOptionString
                                self.navigationController?.pushViewController(instructionsVC, animated: true)
                            }
                                //UserDefaults.standard.set(true, forKey: "show")

                            view.dismiss(animated: true)
                        })
                        let camera = UIAlertAction(title: self.languageChangeString(a_str: "Take a picture"), style: .default, handler: { action in
                            if UserDefaults.standard.bool(forKey: "doNotshowgender") == true{
                                self.imgPicker = UIImagePickerController()
                                if UIImagePickerController.isSourceTypeAvailable(.camera) {
                                    self.camera()
                                } else {
                                    self.noCamera()
                                }
                            }else{
                                let story = UIStoryboard.init(name: "Main", bundle: nil)
                                let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                                  //blogVC.fromVC = "SignUP"
                                instructionsVC.fromViewString = "camera"
                                instructionsVC.fromOptionString = self.fromOptionString
                                self.navigationController?.pushViewController(instructionsVC, animated: true)
                            }
                            view.dismiss(animated: true)
                        })
                        let cancel = UIAlertAction(title:  self.languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
                        })
                        view.addAction(camera)
                        view.addAction(PhotoLibrary)
                        view.addAction(cancel)
                        present(view, animated: true)
                    }else{
                        self.popUpView()
                    }
                }else{
                    self.popUpView()
                }
        
        /*
        self.imgPicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.camera()
        } else {
            self.noCamera()
        }
        */
    }
    
    func camera(){
        self.imgPicker?.delegate = self
        self.imgPicker?.allowsEditing = true
        self.imgPicker?.sourceType = UIImagePickerController.SourceType.camera
        self.imgPicker?.cameraCaptureMode = .photo
        self.imgPicker?.modalPresentationStyle = .fullScreen
        self.imgPicker?.cameraFlashMode = UIImagePickerController.CameraFlashMode.off
        self.present(self.imgPicker!,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: self.languageChangeString(a_str: "No Camera"),message: self.languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: self.languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    //MARK:plantMeterBtnAction
    @IBAction func plantMeterBtnAction(_ sender: Any) {
        self.fromOptionString = "meter"
        if self.membership_status == "1"{
            if self.remainedScans >= 1{
                let view = UIAlertController(title: self.languageChangeString(a_str: ""), message: self.languageChangeString(a_str: "Take a picture or Choose a picture from library"), preferredStyle: .actionSheet)
                let PhotoLibrary = UIAlertAction(title: self.languageChangeString(a_str: "Choose a photo from your library"), style: .default, handler: { action in
                    //self.PhotoLibrary()
                     if UserDefaults.standard.bool(forKey: "doNotshowmeter") == true{
                         self.PhotoLibrary()
                     }else{
                         let story = UIStoryboard.init(name: "Main", bundle: nil)
                         let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                           //blogVC.fromVC = "SignUP"
                         instructionsVC.fromViewString = ""
                         instructionsVC.fromOptionString = self.fromOptionString
                         self.navigationController?.pushViewController(instructionsVC, animated: true)
                     }
                    view.dismiss(animated: true)
                })
                let camera = UIAlertAction(title: self.languageChangeString(a_str: "Take a picture"), style: .default, handler: { action in
                     if UserDefaults.standard.bool(forKey: "doNotshowmeter") == true{
                         self.imgPicker = UIImagePickerController()
                         if UIImagePickerController.isSourceTypeAvailable(.camera) {
                             self.camera()
                         } else {
                             self.noCamera()
                         }
                     }else{
                         let story = UIStoryboard.init(name: "Main", bundle: nil)
                         let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                           //blogVC.fromVC = "SignUP"
                         instructionsVC.fromViewString = "camera"
                         instructionsVC.fromOptionString = self.fromOptionString
                         self.navigationController?.pushViewController(instructionsVC, animated: true)
                     }
                    view.dismiss(animated: true)
                })
                let cancel = UIAlertAction(title:  self.languageChangeString(a_str: "Cancel"), style: .cancel, handler: { action in
                })
                view.addAction(camera)
                view.addAction(PhotoLibrary)
                view.addAction(cancel)
                present(view, animated: true)
            }else{
                self.popUpView()
            }
        }else{
            self.popUpView()
        }
    }
    
    //MARK: PHOTO LIBRARY
    func PhotoLibrary(){
        self.imgPicker?.allowsEditing = true
        self.imgPicker = UIImagePickerController()
        self.imgPicker?.delegate = self
        self.imgPicker?.sourceType = .photoLibrary
        self.present(self.imgPicker!, animated: true)
    }
    
    @IBAction func loginBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let SignInVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        SignInVC.guestCheckString = "guest"
        self.navigationController?.pushViewController(SignInVC, animated: true)
    }
    
    @IBAction func galleryBtnAction(_ sender: Any) {
//        imgPicker?.delegate = self
//        if let image = imgPicker {
//            getImage(picker: image)
//        }
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "BlogPostVC") as! BlogPostVC
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    
    @IBAction func historyBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let historyVC = story.instantiateViewController(withIdentifier: "HistoryViewController") as! HistoryViewController
        historyVC.sideCheckString = ""
        self.navigationController?.pushViewController(historyVC, animated: true)
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let historyVC = story.instantiateViewController(withIdentifier: "MembershipPlansVC") as! MembershipPlansVC
//       // historyVC.sideCheckString = ""
//        self.navigationController?.pushViewController(historyVC, animated: true)
    }
    
    // MARK: Get Image from Camera or Gallery
    func getImage(picker: UIImagePickerController) {
        let alert = UIAlertController(title: "", message: self.languageChangeString(a_str: "OPTIONS"), preferredStyle: .actionSheet)
        
        let type = [self.languageChangeString(a_str: "Camera"),self.languageChangeString(a_str: "Gallery")]
        
        for index in 0..<type.count {
            let button = UIAlertAction(title: type[index], style: .default, handler: { (action) in
                if index == 0 {
                    self.openCamera(picker: picker)
                } else {
                    
                    self.openGallary(picker: picker)
                }
            })
            alert.addAction(button)
        }
        let cancel = UIAlertAction(title: self.languageChangeString(a_str: "Cancel"), style: .cancel) { (action) in }
        alert.addAction(cancel)
        self.present(alert, animated: true) { }
    }
    // MARK: ActionSheet
    
    func openGallary(picker: UIImagePickerController?) {
        if let imagePicker = picker {
            imagePicker.allowsEditing = true
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: Open Camera to take Image
    func openCamera(picker: UIImagePickerController?) {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            if let imgpick = picker {
                imgpick.allowsEditing = true
                imgpick.sourceType = UIImagePickerController.SourceType.camera
                imgpick.cameraCaptureMode = .photo
                self.present(imgpick, animated: true, completion: nil)
            }
        } else {
            self.presentAlertWithTitle(title: self.languageChangeString(a_str: "Alert!")!, message: self.languageChangeString(a_str: "No Camera")!, options: self.languageChangeString(a_str: "OK")!, completion: { (option) in
            })
        }
    }

    @IBAction func scanBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let galleryVC = story.instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
          tabBarController?.hidesBottomBarWhenPushed = true
        //cameraVC.checkPush = "Home"
        self.navigationController?.pushViewController(galleryVC, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "userId") != nil{
            //self.title_lbl.text = String(format: "%@ %@,",self.languageChangeString(a_str: "Hello")!,UserDefaults.standard.object(forKey: "username") as! CVarArg )
            self.loginBtn.isHidden = true
            self.historyBtn.isHidden = false
            self.tabBarController?.tabBar.isHidden = false
        }else{
            //if UserDefaults.standard.bool(forKey: "isGuest") == true{
            //self.title_lbl.text = self.languageChangeString(a_str: "Hello Stay Anonymous,")
                self.loginBtn.isHidden = false
                self.historyBtn.isHidden = true
                self.tabBarController?.tabBar.isHidden = false
//            }else{
//                self.title_lbl.text = "Hello,"
//                self.loginBtn.isHidden = false
//                self.historyBtn.isHidden = true
//                self.tabBarController?.tabBar.isHidden = false
//            }
        }
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = false
    }
     
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:- MENU BTN ACTION
    @IBAction func menuBtnAction(_ sender: Any) {
         present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
  
    //MARK: membershipCheckServiceCall
    func membershipCheckServiceCall(){
        if Reachability.isConnectedToNetwork() {
           // ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/membershipCheck?user_id=181
            //            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            //                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            //            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["user_id":self.userIdString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: membershipCheckAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of membershipCheckAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                self.remainedScans = responseData["remained_scans"] as? Int
                self.membership_status = responseData["membership_status"] as? String
                if self.membership_status == "1"{
                    UserDefaults.standard.set(true, forKey: "membership")
                }else if self.membership_status == "0"{
                    UserDefaults.standard.set(false, forKey: "membership")
                }
                if status == 1{
                    //ServicesHelper.sharedInstance.dissMissLoader()

                }else{
//                    let message = responseData["message"] as! String
//                    ServicesHelper.sharedInstance.dissMissLoader()
//                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
}

// MARK: - ImagePickerController Delegate and NavigationController Delegate
extension HomeVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        
        //picker.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        scanVC.fromOptionString = self.fromOptionString
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
