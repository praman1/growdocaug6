//
//  UsePictureViewController.swift
//  GrowDoc
//
//  Created by volivesolutions on 10/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class UsePictureViewController: UIViewController  {
fileprivate var imgPicker: UIImagePickerController? = UIImagePickerController()
    var pickedImage  = UIImage()
    var window : UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        self.imgPicker?.delegate = self
//        if let imagePicker = self.imgPicker {
//            imagePicker.allowsEditing = true
//            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
//            self.present(imagePicker, animated: true, completion: nil)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.bool(forKey: "membership") == false{
            self.popUp()
        }else{
            self.imgPicker?.delegate = self
            if let imagePicker = self.imgPicker {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func popUp(){
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PopUpVC = story.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
        var viewcontroller = UIApplication.shared.keyWindow?.rootViewController
        let navigation = UINavigationController.init(rootViewController: PopUpVC )
        PopUpVC.fromViewString = "gallery"
        navigation.modalPresentationStyle = .overCurrentContext
        while ((viewcontroller?.presentedViewController) != nil)
        {
            viewcontroller = viewcontroller?.presentedViewController
        }
        viewcontroller?.present(navigation, animated: true, completion: nil)
    }
}

// MARK: - ImagePickerController Delegate and NavigationController Delegate
extension UsePictureViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
//        self.window?.rootViewController = initialViewController
//        self.window?.makeKeyAndVisible()
        
//
//        let presentingVC = self.presentingViewController
//        self.dismiss(animated: true) {
//            let story = UIStoryboard.init(name: "Main", bundle: nil)
//            let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//
//
//            let nav = tabVC.viewControllers?[0] as! UINavigationController
//            let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//            tabVC.selectedIndex = 0
//            self.present(tabVC, animated: true) {
//                nav.pushViewController(storeVC, animated: false)
//            }
//         }
        
//        let presentingVC = self.presentingViewController
//        self.dismiss(animated: true) {
//
//            self.navigationController?.popToRootViewController(animated: true)
//
////            let story = UIStoryboard.init(name: "Main", bundle: nil)
////            let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
////
////
////            let nav = tabVC.viewControllers?[0] as! UINavigationController
////            let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
////
////            tabVC.selectedIndex = 0
////            presentingVC!.dismiss(animated: true, completion: nil)
////            self.present(tabVC, animated: true) {
////                nav.pushViewController(storeVC, animated: false)
////            }
//        }
//
        
        
       //self.dismiss(animated: true, completion: nil)
        
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//        
//        
//        let nav = tabVC.viewControllers?[0] as! UINavigationController
//        let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        
//        tabVC.selectedIndex = 0
//        
//        self.present(tabVC, animated: true) {
//            nav.pushViewController(storeVC, animated: false)
//        }
        
        
        
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//
//        let homeVC = story.instantiateViewController(withIdentifier: "HomeVC1")
//        self.present(homeVC, animated: true, completion: nil)
        //self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        
        //picker.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
