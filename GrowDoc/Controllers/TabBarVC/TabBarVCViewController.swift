//
//  TabBarVCViewController.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class TabBarVCViewController: UITabBarController {
    static private(set) var currentInstance: TabBarVCViewController?
    var tabBarItems = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("reloadTabBar"), object: nil)
        self.tabBarItems = [self.languageChangeString(a_str: "Home"),self.languageChangeString(a_str: "Upload a photo")] as! [String]
        TabBarVCViewController.currentInstance = self
        let selectedColor   = #colorLiteral(red: 0.3308642507, green: 0.5630635023, blue: 0.0006377805839, alpha: 1)
        let unselectedColor = #colorLiteral(red: 0.3450980392, green: 0.3450980392, blue: 0.3450980392, alpha: 1)
        self.navigationController?.isNavigationBarHidden = true
        guard let items = tabBar.items else { return }
        items[0].title = self.languageChangeString(a_str: "Home")
        items[1].title = self.languageChangeString(a_str: "Scan your plant")
        items[2].title = self.languageChangeString(a_str: "Upload a photo")
        
        
//        self.tabBar.layer.masksToBounds = true
//
//        //self.tabBar.tintColor = UIColor.clear
//        self.tabBar.layer.cornerRadius = 30
//        if #available(iOS 11.0, *) {
//            self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
//            tabBar.barTintColor = UIColor.white
//            //tabBar.isTranslucent = false
//
//        } else {
//            // Fallback on earlier versions
//        }
        
        
        //UITabBar.appearance().backgroundImage =  #imageLiteral(resourceName: "Details_2")
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor:selectedColor], for: .selected)
    }
    /*
    override func viewWillLayoutSubviews() {
        var newTabBarFrame = tabBar.frame

        let newTabBarHeight: CGFloat = 75
        newTabBarFrame.size.height = newTabBarHeight
        newTabBarFrame.origin.y = self.view.frame.size.height - newTabBarHeight

        tabBar.frame = newTabBarFrame
    }
    */
    @objc func methodOfReceivedNotification(notification: Notification) {
        guard let items = tabBar.items else { return }
        items[0].title = self.languageChangeString(a_str: "Home")
        items[1].title = self.languageChangeString(a_str: "Scan your plant")
        items[2].title = self.languageChangeString(a_str: "Upload a photo")
    }
}
