//
//  InformationPopUpVC.swift
//  GrowDoc
//
//  Created by Apple on 21/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class InformationPopUpVC: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    var languageString : String! = ""
    var infoTextString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.infoTextString
        //self.membershipInfoServiceCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    @IBAction func closeBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
  


}
