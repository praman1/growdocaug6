//
//  MyMembershipVC.swift
//  GrowDoc
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class MyMembershipVC: UIViewController,UITableViewDataSource,UITableViewDelegate,PayPalPaymentDelegate {
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
//    #if HAS_CARDIO
//    // You should use the PayPal-iOS-SDK+card-Sample-App target to enable this setting.
//    // For your apps, you will need to link to the libCardIO and dependent libraries. Please read the README.md
//    // for more details.
//    var acceptCreditCards: Bool = true {
//    didSet {
//    payPalConfig.acceptCreditCards = acceptCreditCards
//    
//    }
//    }
//    #else
//    var acceptCreditCards: Bool = false {
//        didSet {
//            payPalConfig.acceptCreditCards = acceptCreditCards
//        }
//    }
//    #endif
    var payPalConfig = PayPalConfiguration()
    var resultText = "" // empty
    var languageString : String! = ""
    
    var userIdString : String! = ""
    var myMembsershipsArray = [MyMembershipModel]()
    var imageArray = [String]()
    var repeatCount : Int! = 0
    var fromViewString : String! = ""
    
    var priceString : String! = ""
    var transactionId : String! = ""
    var status = ""
   // var paymentRequest: PKPaymentRequest!
    var membershipIdString : String! = ""
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var myMembershipsTableView: UITableView!
    
    
    @IBOutlet weak var purchaseBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userMembsershipsAPICall()
        self.titleLabel.text = self.languageChangeString(a_str: "My Membership")
        self.purchaseBtn.setTitle(self.languageChangeString(a_str: "Other Memberships"), for: UIControl.State.normal)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationController?.isNavigationBarHidden = true
        payPalConfig.acceptCreditCards = true
        PayPalMobile.preconnect(withEnvironment: environment)
    }

    @IBAction func backBtnActon(_ sender: Any) {
        if self.fromViewString == "success"{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
            homeVC.modalPresentationStyle = .fullScreen
            self.present(homeVC, animated:true, completion: nil)
        }else{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func purchaseBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let PlansVC = story.instantiateViewController(withIdentifier: "MembershipPlansVC") as! MembershipPlansVC
        self.navigationController?.pushViewController(PlansVC, animated: true)
    }
    
    //MARK : buyNowClicked
    @objc func buyNowClicked(sender : UIButton){

        let selectedIndex = sender.tag
        self.membershipIdString = self.myMembsershipsArray[selectedIndex].id
       // let total = self.plansArray[selectedIndex].price.replacingOccurrences(of: "$", with: "", options: String.CompareOptions.literal, range: nil)
        self.priceString = self.myMembsershipsArray[selectedIndex].amount
        let payment = PayPalPayment(amount: NSDecimalNumber(string: self.priceString), currencyCode: "CAD", shortDescription: "Membership Purchase", intent: .sale)
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: self.payPalConfig, delegate: self)
             paymentViewController!.modalPresentationStyle = .fullScreen
            self.present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // This particular payment will always be processable. If, for
            // example, the amount was negative or the shortDescription was
            // empty, this payment wouldn't be processable, and you'd want
            // to handle that here.
            print("Payment not processalbe: \(payment)")
        }
        
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        //successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            let response = completedPayment.confirmation
            print("Response:::\(response)")
            
            let dict = response["response"] as! [String:AnyObject]
            print("Dict of response:::\(dict)")
            
            self.transactionId = dict["id"] as? String
            print("TransactionId:::\(self.transactionId)")
            self.purchaseMembershipAPICall()
            self.resultText = completedPayment.description
            self.showSuccess()
        })
    }

    func showSuccess() {
        //successView.isHidden = false
       // successView.alpha = 1.0
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationDelay(2.0)
        //successView.alpha = 0.0
        UIView.commitAnimations()
    }
    
    //MARK : userMembsershipsAPICall
    func userMembsershipsAPICall(){
      if Reachability.isConnectedToNetwork() {
          ServicesHelper.sharedInstance.loader(view: self.view)
          //http://demomaplebrains.com/growdoc_phase/services/userMemberships?user_id=181
          if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
              self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
          }
          if UserDefaults.standard.object(forKey: "userId") != nil{
              self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
          }
        let postDict = ["lang":self.languageString,"user_id":self.userIdString]
          ServicesHelper.sharedInstance.getDataServiceCall(url: userMembershipsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
              print("Response of userMembershipsAPI",responseData)
              let status : Int = Int(responseData["status"] as! Int)
              if status == 1{
                  ServicesHelper.sharedInstance.dissMissLoader()
                  self.myMembsershipsArray = [MyMembershipModel]()
                  self.imageArray = [String]()
                  var count : Int = 0
                  if let memberships = responseData["data"] as? [[String : Any]]{
                      for membership in memberships{
                          let membershipObject = MyMembershipModel(membershipData: membership as NSDictionary)
                          self.myMembsershipsArray.append(membershipObject)
                          count = count+1
                        self.repeatCount = count
                      }
                      print("Repeat Count :::\(self.repeatCount)")
                      self.imageArray = ["DarkGreen","LightGreen","Yellow"].repeated(count: self.repeatCount)
                      print("Image Array:::\(self.imageArray)")
                  }
                  DispatchQueue.main.async {
                      self.myMembershipsTableView.reloadData()
                  }
              }else{
                  ServicesHelper.sharedInstance.dissMissLoader()
                  let message = responseData["message"] as! String
                  self.showToastForAlert(message: message)
              }
          }
      }else {
          ServicesHelper.sharedInstance.dissMissLoader()
          self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
          //self.showToastForAlert(message: "Please check your Internet Connection")
      }
      
  }
    
    //MARK : purchaseMembershipAPICall
    func purchaseMembershipAPICall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/membership_plans?lang=en
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["lang":self.languageString,"user_id":self.userIdString,"membership_id":self.membershipIdString,"transaction_id":self.transactionId]
            ServicesHelper.sharedInstance.getDataServiceCall(url: purchaseMembershipAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of purchaseMembershipAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToast(message: message)
                    let story = UIStoryboard.init(name: "Main", bundle: nil)
                    let signVC = story.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                    self.navigationController?.pushViewController(signVC, animated: true)
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    //MARK: TableView Delegate Methods
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.myMembsershipsArray.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.myMembershipsTableView.dequeueReusableCell(withIdentifier: "MyMembershipTableViewCell", for: indexPath) as! MyMembershipTableViewCell
        let membershipObject = self.myMembsershipsArray[indexPath.row]
        
        cell.backImageView.image = UIImage.init(named: self.imageArray[indexPath.row])
        cell.membershipNameLabel.text = membershipObject.membership_name
        cell.selectedMembershipLabel.text = membershipObject.duration
        cell.priceLabel.text = membershipObject.price
        cell.scansLeftLabel.text = membershipObject.custom_text
            //+ " " + self.languageChangeString(a_str: "Scans are left")!
        if membershipObject.duration == ""{
            cell.tick1ImageView.image = UIImage.init(named: "")
        }else{
            cell.tick1ImageView.image = UIImage.init(named: "Path 14968")
        }
        if membershipObject.price == ""{
            cell.tick2ImageView.image = UIImage.init(named: "")
        }else{
            cell.tick2ImageView.image = UIImage.init(named: "Path 14968")
        }
        if membershipObject.custom_text == ""{
            cell.tick4ImageView.image = UIImage.init(named: "")
        }else{
            cell.tick4ImageView.image = UIImage.init(named: "Path 14968")
        }
        if membershipObject.contact_support == "0"{
            cell.tick5ImageView.image = UIImage.init(named: "")
            cell.supportLabel.text = ""
        }else{
            cell.tick5ImageView.image = UIImage.init(named: "Path 14968")
            cell.supportLabel.text = self.languageChangeString(a_str: "Ability to ask GrowDoc team for help")
        }
        if membershipObject.expires_in == "" {
            cell.dateLabel.text = self.languageChangeString(a_str: "Expired")
            cell.buyNowBtn.isHidden = false
            cell.expireDaysLabel.text = ""
            cell.tick3ImageView.image = UIImage.init(named: "")
        }else{
            cell.tick3ImageView.image = UIImage.init(named: "Path 14968")
            cell.dateLabel.text = membershipObject.expired_date
            cell.buyNowBtn.isHidden = true
            cell.expireDaysLabel.text = membershipObject.expires_in
        }
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                cell.buttonWidthConstraint.constant = 90
                print("ENGLISH_LANGUAGE")
             }else{
                cell.buttonWidthConstraint.constant = 130
                print("FRENCH_LANGUAGE")
             }
        }else{
            cell.buttonWidthConstraint.constant = 90
            print("ELSE ENGLISH_LANGUAGE")
        }
        cell.buyNowBtn.setTitle(self.languageChangeString(a_str: "BUY NOW"), for: UIControl.State.normal)
        cell.buyNowBtn.layer.borderColor = UIColor.white.cgColor
        cell.buyNowBtn.layer.borderWidth = 1.0
        cell.buyNowBtn.tag = indexPath.row
        cell.buyNowBtn.addTarget(self, action: #selector(buyNowClicked(sender :)), for: UIControl.Event.touchUpInside)
        
        // Configure the cell...

        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200.0
    }

}
