//
//  PopUpVC.swift
//  GrowDoc
//
//  Created by Apple on 13/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class PopUpVC: UIViewController {
    fileprivate var imgPicker: UIImagePickerController? = UIImagePickerController()
    var pickedImage  = UIImage()
    
    @IBOutlet weak var timesLabel: UILabel!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var skipbtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var membershipBtn: UIButton!
    var window: UIWindow?
    var fromViewString : String! = ""
    var fromOptionString : String! = ""
    var languageString : String! = ""
    var seconds = 30
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.titleLabel.text = self.languageChangeString(a_str: "Go for Premium Membership for  immediate and accurate results")
        self.membershipBtn.setTitle(self.languageChangeString(a_str: "Get Premium"), for: UIControl.State.normal)
        self.skipbtn.setTitle(self.languageChangeString(a_str: "Skip"), for: UIControl.State.normal)
        self.membershipInfoServiceCall()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if self.fromOptionString == "gender" || self.fromOptionString == "meter"{
            self.timesLabel.text = self.languageChangeString(a_str: "Premium membership is required to use this feature")
            //self.timesLabel.textColor = .black
        }else{
            self.runTimer()
            //self.timesLabel.textColor = UIColor.init(red: 84.0/255.0, green: 144.0/255.0, blue: 0.0/255.0, alpha: 1.0)
        }
    }
    
    
    func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(AdsVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        self.timesLabel.text = String(format: "%@ %d %@,%@",self.languageChangeString(a_str:
            "This pop up will close in")!,seconds,self.languageChangeString(a_str: "seconds")!,self.languageChangeString(a_str: "or you can click X to continue")!)
       // print("Running Time:::",seconds)
        if seconds == 0{
            self.stopTimer()
            //self.dismiss(animated: true, completion: nil)
        }
        //"\(seconds)" //This will update the label.
    }
    
    func stopTimer(){
        self.timer.invalidate()
        if self.fromOptionString == "gender" || self.fromOptionString == "meter"{
            self.dismiss(animated: true, completion: nil)
        }else{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
            homeVC.modalPresentationStyle = .fullScreen
            self.present(homeVC, animated:true, completion: nil)
        }
        
    }
    
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.timer.invalidate()
        if self.fromOptionString == "gender" || self.fromOptionString == "meter"{
//            let story = UIStoryboard.init(name: "Main", bundle: nil)
//            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//            homeVC.modalPresentationStyle = .fullScreen
//            self.present(homeVC, animated:true, completion: nil)
            self.dismiss(animated: true, completion: nil)
        }else{
            if UserDefaults.standard.bool(forKey: "doNotshowscan") == true{
                if self.fromViewString == "camera"{
                    self.imgPicker = UIImagePickerController()
                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        self.camera()
                    } else {
                        self.noCamera()
                    }
                }else{
                    //self.dismiss(animated: true, completion: nil)
                    self.imgPicker?.delegate = self
                    if let imagePicker = self.imgPicker {
                        imagePicker.allowsEditing = true
                        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                        imagePicker.modalPresentationStyle = .fullScreen
                        self.present(imagePicker, animated: true, completion: nil)
                    }
                }
            }else{
                let story = UIStoryboard.init(name: "Main", bundle: nil)
                let instructionsVC = story.instantiateViewController(withIdentifier: "InstructionsVC") as! InstructionsVC
                  //blogVC.fromVC = "SignUP"
                instructionsVC.fromViewString = self.fromViewString
                instructionsVC.fromOptionString = self.fromOptionString
                self.navigationController?.pushViewController(instructionsVC, animated: true)
            }
        }
       /*
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        */
    }
    
    @IBAction func skipBtnAction(_ sender: Any) {
        if self.fromViewString == "camera"{
            self.imgPicker = UIImagePickerController()
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                self.camera()
            } else {
                self.noCamera()
            }
        }else{
            //self.dismiss(animated: true, completion: nil)
            self.imgPicker?.delegate = self
            if let imagePicker = self.imgPicker {
                imagePicker.allowsEditing = true
                imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                imagePicker.modalPresentationStyle = .fullScreen
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func camera(){
        self.imgPicker?.delegate = self
        self.imgPicker?.allowsEditing = true
        self.imgPicker?.sourceType = UIImagePickerController.SourceType.camera
        self.imgPicker?.cameraCaptureMode = .photo
        self.imgPicker?.modalPresentationStyle = .fullScreen
        self.imgPicker?.cameraFlashMode = UIImagePickerController.CameraFlashMode.off
        self.present(self.imgPicker!,animated: true,completion: nil)
    }
    
    func noCamera(){
        let alertVC = UIAlertController(
            title: self.languageChangeString(a_str: "No Camera"),message: self.languageChangeString(a_str: "Sorry, this device has no camera"),preferredStyle: .alert)
        let okAction = UIAlertAction(
            title: self.languageChangeString(a_str: "OK"),style:.default,handler: nil)
        alertVC.addAction(okAction)
        present(
            alertVC,animated: true,completion: nil)
    }
    
    @IBAction func membershipBtnAction(_ sender: Any) {
        self.timer.invalidate()
        if UserDefaults.standard.bool(forKey: "isGuest") == true{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let SignInVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
            SignInVC.fromViewString = "guestMem"
            self.navigationController?.pushViewController(SignInVC, animated: true)
        }else{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let PlansVC = story.instantiateViewController(withIdentifier: "MembershipPlansVC") as! MembershipPlansVC
            self.navigationController?.pushViewController(PlansVC, animated: true)
        }
    }
    
    func membershipInfoServiceCall(){
            if Reachability.isConnectedToNetwork() {
                //ServicesHelper.sharedInstance.loader(view: self.view)
                //http://demomaplebrains.com/growdoc_phase/services/membership_popup?lang=
                if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                    self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
                }
                let postDict = ["lang" : self.languageString]
                ServicesHelper.sharedInstance.getDataServiceCall(url: membership_popup ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                    let status : Int = Int(responseData["status"] as! Int)
                    if status == 1{
                        //ServicesHelper.sharedInstance.dissMissLoader()
                        if let data = responseData["data"] as? [String: Any] {

                            if data["image"] as? String != ""{
                                let image = data["image"] as? String
                                let imageString : String = BASE_PATH + image!
                                self.infoImageView.setKFImage(with: imageString)
                               // self.infoImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                                //self.imageHeight.constant  = 200
                            }else{
                                //self.imageHeight.constant  = 0
                            }
                        }
                    }else{
                        let message = responseData["message"] as! String
                        //ServicesHelper.sharedInstance.dissMissLoader()
                        self.showToastForAlert(message: message)
                    }
                }
            }else {
                //ServicesHelper.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            }
        }
}

// MARK: - ImagePickerController Delegate and NavigationController Delegate
extension PopUpVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
        
//        self.window = UIWindow(frame: UIScreen.main.bounds)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
//        self.window?.rootViewController = initialViewController
//        self.window?.makeKeyAndVisible()
        
//
//        let presentingVC = self.presentingViewController
//        self.dismiss(animated: true) {
//            let story = UIStoryboard.init(name: "Main", bundle: nil)
//            let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//
//
//            let nav = tabVC.viewControllers?[0] as! UINavigationController
//            let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//            tabVC.selectedIndex = 0
//            self.present(tabVC, animated: true) {
//                nav.pushViewController(storeVC, animated: false)
//            }
//         }
        
//        let presentingVC = self.presentingViewController
//        self.dismiss(animated: true) {
//
//            self.navigationController?.popToRootViewController(animated: true)
//
////            let story = UIStoryboard.init(name: "Main", bundle: nil)
////            let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
////
////
////            let nav = tabVC.viewControllers?[0] as! UINavigationController
////            let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
////
////            tabVC.selectedIndex = 0
////            presentingVC!.dismiss(animated: true, completion: nil)
////            self.present(tabVC, animated: true) {
////                nav.pushViewController(storeVC, animated: false)
////            }
//        }
//
        
        
       //self.dismiss(animated: true, completion: nil)
        
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let tabVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//
//
//        let nav = tabVC.viewControllers?[0] as! UINavigationController
//        let storeVC = story.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//
//        tabVC.selectedIndex = 0
//
//        self.present(tabVC, animated: true) {
//            nav.pushViewController(storeVC, animated: false)
//        }
        
        
        
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//
//        let homeVC = story.instantiateViewController(withIdentifier: "HomeVC1")
//        self.present(homeVC, animated: true, completion: nil)
        //self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        self.pickedImage  = UIImage()
        if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
            print("editedImage Is:\(editedImage)")
            self.pickedImage = editedImage
        }
        else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            print("originalImage Is:\(originalImage)")
            self.pickedImage = originalImage
        }
        print("Picked Image Is:\(pickedImage)")
        
        //picker.dismiss(animated: true, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let scanVC = story.instantiateViewController(withIdentifier: "ScanVC") as! ScanVC
        tabBarController?.hidesBottomBarWhenPushed = true
        scanVC.postScanImage = self.pickedImage
        scanVC.fromOptionString = self.fromOptionString
        self.navigationController?.pushViewController(scanVC, animated: true)
        picker.dismiss(animated: true, completion: nil)
    }
}
