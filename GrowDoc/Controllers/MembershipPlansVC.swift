//
//  MembershipPlansVC.swift
//  GrowDoc
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit
import PassKit
class MembershipPlansVC: UIViewController,UITableViewDataSource,UITableViewDelegate,PayPalPaymentDelegate{
    
    var environment:String = PayPalEnvironmentSandbox {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    var payPalConfig = PayPalConfiguration()
    var resultText = "" // empty
//PKPaymentAuthorizationViewControllerDelegate
    @IBOutlet weak var plansHeaderLabel: UILabel!
    @IBOutlet weak var plansTableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    var plansArray = [MembershipPlansModel]()
    var imageArray = [String]()
    var languageString : String! = ""
    var userIdString : String! = ""
    var repeatCount : Int! = 0
    var amount = 1
    var priceString : String! = ""
    var transactionId : String! = ""
    var status = ""
    var paymentRequest: PKPaymentRequest!
    var membershipIdString : String! = ""
    var fourDigitNumber: String {
     var result = ""
     repeat {
         // Create a string with a random number 0...9999
         result = String(format:"%05d", arc4random_uniform(100000) )
     } while Set<Character>(result.characters).count < 4
     return result
    }
   // var transactionIdString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.imageArray = ["Subtraction 15","Subtraction 14","Subtraction 17"]
       // self.imageArray = ["DarkGreen","LightGreen","Yellow"]
        //,"Yellow"
        // USAGE

        self.membershipPlansAPICall()
        self.titleLabel.text = self.languageChangeString(a_str: "Membership")
        self.plansHeaderLabel.text = self.languageChangeString(a_str: "Membership plans")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        payPalConfig.acceptCreditCards = true
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - membershipPlansAPICall
    func membershipPlansAPICall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/membership_plans?lang=en
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: membershipPlansAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of membershipPlansAPI",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.plansArray = [MembershipPlansModel]()
                    self.imageArray = [String]()
                    var count : Int = 0
                    if let plans = responseData["data"] as? [[String : Any]]{
                        for plan in plans{
                            let planObject = MembershipPlansModel(plansData: plan as NSDictionary)
                            self.plansArray.append(planObject)
                            count = count+1
                            self.repeatCount = count
                        }
                        print("Repeat Count :::\(self.repeatCount)")
                        self.imageArray = ["DarkGreen","LightGreen","Yellow"].repeated(count: self.repeatCount)
                        print("Image Array:::\(self.imageArray)")
                    }
                    DispatchQueue.main.async {
                        self.plansTableView.reloadData()
                    }
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
        
    }
    
    //MARK : purchaseMembershipAPICall
    func purchaseMembershipAPICall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/membership_plans?lang=en
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["lang":self.languageString,"user_id":self.userIdString,"membership_id":self.membershipIdString,"transaction_id":self.transactionId]
            ServicesHelper.sharedInstance.getDataServiceCall(url: purchaseMembershipAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of purchaseMembershipAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToast(message: message)
                    let story = UIStoryboard.init(name: "Main", bundle: nil)
                    let signVC = story.instantiateViewController(withIdentifier: "SuccessVC") as! SuccessVC
                    self.navigationController?.pushViewController(signVC, animated: true)

                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    //MARK : buyNowClicked
    @objc func buyNowClicked(sender : UIButton){
        //self.applePay()
        let selectedIndex = sender.tag
       // self.purchaseMembershipAPICall(membershipId: self.plansArray[selectedIndex].membership_id)
        self.membershipIdString = self.plansArray[selectedIndex].membership_id
        let total = self.plansArray[selectedIndex].price.replacingOccurrences(of: "CAD", with: "", options: String.CompareOptions.literal, range: nil)
        self.priceString = total
        let intValuePrice = (self.priceString as NSString).integerValue
        print("intValuePrice::::::",intValuePrice)
        if self.priceString != "0.00"{
            let payment = PayPalPayment(amount: NSDecimalNumber(string: self.priceString), currencyCode: "CAD", shortDescription: "Membership Purchase", intent: .sale)
            
            if (payment.processable) {
                let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: self.payPalConfig, delegate: self)
                paymentViewController!.modalPresentationStyle = .fullScreen
                self.present(paymentViewController!, animated: true, completion: nil)
            }
            else {
                // This particular payment will always be processable. If, for
                // example, the amount was negative or the shortDescription was
                // empty, this payment wouldn't be processable, and you'd want
                // to handle that here.
                print("Payment not processalbe: \(payment)")
            }
        }else{
            self.transactionId = fourDigitNumber
            print("Generated Password : \(self.transactionId ?? "")")
            self.purchaseMembershipAPICall()
        }
//        if intValuePrice > 0{
//
//        }else{
//
//        }
    }

    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        print("PayPal Payment Cancelled")
        resultText = ""
        //successView.isHidden = true
        paymentViewController.dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        print("PayPal Payment Success !")
        paymentViewController.dismiss(animated: true, completion: { () -> Void in
            // send completed confirmaion to your server
            print("Here is your proof of payment:\n\n\(completedPayment.confirmation)\n\nSend this to your server for confirmation and fulfillment.")
            let response = completedPayment.confirmation
            print("Response:::\(response)")
            
            let dict = response["response"] as! [String:AnyObject]
            print("Dict of response:::\(dict)")
            
            self.transactionId = dict["id"] as? String
            print("TransactionId:::\(self.transactionId)")
            self.purchaseMembershipAPICall()
            self.resultText = completedPayment.description
            self.showSuccess()
        })
    }

    func showSuccess() {
        //successView.isHidden = false
       // successView.alpha = 1.0
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.5)
        UIView.setAnimationDelay(2.0)
        //successView.alpha = 0.0
        UIView.commitAnimations()
    }
    
    func applePay(){
        let request = PKPaymentRequest()
        request.merchantIdentifier = "merchant.com.GrowDocApp"
        request.supportedNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard, PKPaymentNetwork.amex]
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        
        request.paymentSummaryItems = [
            PKPaymentSummaryItem(label: "Some Product", amount: 9.99)
        ]
        
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        applePayController?.delegate = self
        self.present(applePayController!, animated: true, completion: nil)
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.plansArray.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.plansTableView.dequeueReusableCell(withIdentifier: "PlansTableViewCell", for: indexPath) as! PlansTableViewCell
        let plansObject = self.plansArray[indexPath.row]
        cell.backImageView.image = UIImage.init(named: self.imageArray[indexPath.row])
        cell.membershipNameLabel.text = plansObject.membership_name
        cell.durationLabel.text = plansObject.duration
        cell.noOfScansLabel.text = plansObject.scans
        cell.priceLabel.text = "CAD" + plansObject.price
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                cell.buttonWidthConstraint.constant = 108
                print("ENGLISH_LANGUAGE")
             }else{
                cell.buttonWidthConstraint.constant = 150
                print("FRENCH_LANGUAGE")
             }
        }else{
            cell.buttonWidthConstraint.constant = 108
            print("ELSE ENGLISH_LANGUAGE")
        }
        if plansObject.contact_support == "0"{
            cell.supportLabel.text = ""
        }else{
            cell.supportLabel.text = self.languageChangeString(a_str: "Ability to ask GrowDoc team for help")
        }
        cell.customNameLabel.text = plansObject.custom_text
        cell.buyNowBtn.setTitle(self.languageChangeString(a_str: "BUY NOW"), for: UIControl.State.normal)
        cell.buyNowBtn.layer.borderColor = UIColor.white.cgColor
        cell.buyNowBtn.layer.borderWidth = 1.0
        cell.buyNowBtn.tag = indexPath.row
        cell.buyNowBtn.addTarget(self, action: #selector(buyNowClicked(sender :)), for: UIControl.Event.touchUpInside)
        // Configure the cell...

        return cell
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 205.0
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension MembershipPlansVC:
PKPaymentAuthorizationViewControllerDelegate{
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        completion(PKPaymentAuthorizationResult(status: PKPaymentAuthorizationStatus.success, errors: []))
    }
}

extension Array {
  init(repeating: [Element], count: Int) {
    self.init([[Element]](repeating: repeating, count: count).flatMap{$0})
  }

  func repeated(count: Int) -> [Element] {
    return [Element](repeating: self, count: count)
  }
}
