//
//  DeseaseNotDetectedViewController.swift
//  GrowDoc
//
//  Created by volivesolutions on 17/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class DeseaseNotDetectedViewController: UIViewController {

    @IBOutlet var showMoreView: UIView!
    @IBOutlet var scannedImageView: UIImageView!
    @IBOutlet var errorMessageLabel: UILabel!
    @IBOutlet var communityView: UIView!
    @IBOutlet var supportBtn: UIButton!
    
    
    @IBOutlet weak var moreOptionsLabel: UILabel!
    @IBOutlet weak var askForSupportLabel: UILabel!
    @IBOutlet weak var compareYourLabel: UILabel!
    @IBOutlet weak var compareBtn: UIButton!
    
    var errorMessage : String! = ""
    var scannedImage = UIImage()
    var imageNameString : String! = ""
    var fromOptionString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.fromOptionString == "gender"{
            self.errorMessageLabel.text = self.languageChangeString(a_str: "Sorry,GrowDoc couldn't recognize your")! + " " + self.languageChangeString(a_str: "Plant Male/Female")!
            self.askForSupportLabel.text = self.languageChangeString(a_str: "Talk to support about your")! + " " + self.languageChangeString(a_str: "Plant Male/Female")!
            self.compareYourLabel.text = self.languageChangeString(a_str: "Compare your scanned picture with")! + " " + self.languageChangeString(a_str: "Plant Male/Female")!
        }else if self.fromOptionString == "meter"{
            self.errorMessageLabel.text = self.languageChangeString(a_str: "Sorry,GrowDoc couldn't recognize your")! + " " + self.languageChangeString(a_str: "Plants Health Meter")!
            self.askForSupportLabel.text = self.languageChangeString(a_str: "Talk to support about your")! + " " + self.languageChangeString(a_str: "Plants Health Meter")!
            self.compareYourLabel.text = self.languageChangeString(a_str: "Compare your scanned picture with")! + " " + self.languageChangeString(a_str: "Plants Health Meter")!
        }else{
            self.errorMessageLabel.text = self.languageChangeString(a_str: "Sorry,GrowDoc couldn't recognize your plant problem")
            self.askForSupportLabel.text = self.languageChangeString(a_str: "Talk to support about your")! + " " + self.languageChangeString(a_str: "Plants Problems")!
            self.compareYourLabel.text = self.languageChangeString(a_str: "Compare your scanned picture with")! + " " + self.languageChangeString(a_str: "Plants Problems")!
        }
        self.moreOptionsLabel.text = self.languageChangeString(a_str: "More Options:")
        
        self.supportBtn.setTitle(self.languageChangeString(a_str: "Ask Support"), for: UIControl.State.normal)
        
        self.compareBtn.setTitle(self.languageChangeString(a_str: "Manually Compare"), for: UIControl.State.normal)
        
        self.communityView.addShadow()
        self.showMoreView.addShadow()
        self.scannedImageView.image = self.scannedImage
       // self.errorMessageLabel.text = self.errorMessage
        //self.supportBtn.layer.borderColor = UIColor.init(red: 84.0/255.0, green: 144.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
        //self.supportBtn.layer.borderWidth = 0.70
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
    }
    
    @IBAction func showMoreBtnAction(_ sender: Any) {
        let gallery = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryVC") as! GalleryVC
        gallery.fromOptionString = self.fromOptionString
        self.navigationController?.pushViewController(gallery, animated: true)
    }
    
    @IBAction func askSupportBtnAction(_ sender: Any) {
        if UserDefaults.standard.object(forKey: "userId") != nil{
            if UserDefaults.standard.bool(forKey: "membership") == true{
                let contactUs = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
                contactUs.scannedImage = self.scannedImage
                contactUs.imageNameString = self.imageNameString
                self.navigationController?.pushViewController(contactUs, animated: true)
            }else{
                self.showToastForAlert(message: languageChangeString(a_str: "This is for premium users only")!)
            }
        }else{
            self.showToastForAlert(message: languageChangeString(a_str: "This is for premium users only")!)
        }
//        if UserDefaults.standard.bool(forKey: "isGuest") == true{
//            self.showToastForAlert(message: languageChangeString(a_str: "This feature is for logged in users only")!)
//        }else{
//            let contactUs = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContactUSVC") as! ContactUSVC
//            self.navigationController?.pushViewController(contactUs, animated: true)
//        }
    }
}
