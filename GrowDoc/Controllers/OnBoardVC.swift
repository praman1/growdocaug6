//
//  OnBoardVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SDWebImage
class OnBoardVC: UIViewController {

    @IBOutlet weak var pageIndex: UIPageControl!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var skipBtn: UIButton!
    @IBOutlet weak var onBoardCollectionView: UICollectionView!
    var languageString : String! = ""
    var indexCell: Int = 0
    var onBoardArray = [OnBoardModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.onBoardServiceCall()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
 
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageWidth = scrollView.frame.size.width
        let page = Int(floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1)
        self.pageIndex.currentPage = page
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageWidth: CGFloat = scrollView.frame.size.width
        let fractionalPage = Float(scrollView.contentOffset.x / pageWidth)
        let page: Int = lround(Double(fractionalPage))
       pageIndex.currentPage = page
        
        if page == onBoardheadersArray.count - 1{
            self.nextBtn.setTitle(self.languageChangeString(a_str: "Start"), for: .normal)
 
        }else{
            self.nextBtn.setTitle(self.languageChangeString(a_str: "Next"), for: .normal)
 
        }
    }
    

    @IBAction func skipBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let startVC = story.instantiateViewController(withIdentifier: "StartVC") as! StartVC
        self.navigationController?.pushViewController(startVC, animated: true)
    }
    
    
    @IBAction func nextBtnAction(_ sender: UIButton) {
        let indexOfScrolledCell : NSInteger!
        
        indexOfScrolledCell = indexCell
        if sender.currentTitle == self.languageChangeString(a_str: "Start"){
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let startVC = story.instantiateViewController(withIdentifier: "StartVC") as! StartVC
            self.navigationController?.pushViewController(startVC, animated: true)
        }else{
            if indexCell < 3 {
                var path = IndexPath(row: indexOfScrolledCell + 1, section: 0)
                onBoardCollectionView.scrollToItem(at: path, at: .right, animated: true)
                pageIndex.currentPage = path.row
                
            }
        }
    }
    
    //MARK:ONBOARD SERVICE CALL
    func onBoardServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //http://demomaplebrains.com/growdoc_phase/services/onboarding?lang=en
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: onboardingAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of onboardingAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                //let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.onBoardArray = [OnBoardModel]()
                    if let onBoard = responseData["data"] as? [[String : Any]]{
                        for item in onBoard{
                            let onBoardObject = OnBoardModel(onBoardData: item as NSDictionary)
                            self.onBoardArray.append(onBoardObject)
                        }
                    }
                    self.pageIndex.numberOfPages = self.onBoardArray.count
                    DispatchQueue.main.async {
                         self.onBoardCollectionView.reloadData()
                    }
                    UserDefaults.standard.set(true, forKey: "onboardFirst")
                }else{
                    let message = responseData["message"] as! String
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}

extension  OnBoardVC:  UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.onBoardArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = onBoardCollectionView.dequeueReusableCell(withReuseIdentifier: "OnBoardCollectionViewCell", for: indexPath) as? OnBoardCollectionViewCell
        let onBoardObject = self.onBoardArray[indexPath.row]
        
       cell?.header_lbl.text = onBoardObject.title
       cell?.title_lbl.text = onBoardObject.sub_title
        let imageString : String = BASE_PATH + onBoardObject.image
        cell?.headerImageView.setKFImage(with: imageString)
       //cell?.headerImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
       cell?.description_lbl.text = onBoardObject.description
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: self.onBoardCollectionView.frame.size.width,height:  self.onBoardCollectionView.frame.size.height)
        
        
    }
   
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        indexCell = indexPath.index(after: indexPath.item - 1)
    }
}
