//
//  DetailsVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import Alamofire
import AVKit
import Kingfisher
class DetailsVC: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var scropImageView: UIImageView!
    @IBOutlet weak var diagonisText_lbl: UILabel!
    @IBOutlet weak var plantGenderOrStatusLabel: UILabel!
    @IBOutlet weak var statusValueLabel: UILabel!
    
    @IBOutlet weak var statusViewHeight: NSLayoutConstraint!
    @IBOutlet var solutionLabel: UILabel!
    @IBOutlet weak var confirmDiagonisViewHieghtConstrain: NSLayoutConstraint!
    @IBOutlet weak var showMoreTextLablHeightConstarint: NSLayoutConstraint!
    
    @IBOutlet var videosNotFoundLabel: UILabel!
    @IBOutlet var imagesNotFoundLabel: UILabel!
    @IBOutlet weak var showMoreBtn: UIButton!
    @IBOutlet weak var showMoreText: UILabel!
    @IBOutlet weak var similarImagesCollectionView: UICollectionView!
    @IBOutlet weak var similarVediosCollectionView: UICollectionView!
    
    @IBOutlet var similarImagesCollectionViewGeight: NSLayoutConstraint!
    
    @IBOutlet var similarVideoCollectionViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var matchedDiagnosisHeaderLabel: UILabel!
    @IBOutlet weak var confirmDiagnosisLabel: UILabel!
    @IBOutlet weak var mostSimilarImagesLabel: UILabel!
    @IBOutlet weak var mostSimilarVideosLabel: UILabel!
    @IBOutlet weak var solutiuonsHeaderLabel: UILabel!

    @IBOutlet weak var adImageView1: UIImageView!
    @IBOutlet weak var adImageView2: UIImageView!
    @IBOutlet weak var adImageView3: UIImageView!
    
    @IBOutlet weak var adImageViewHeight1: NSLayoutConstraint!
    @IBOutlet weak var adImageViewHeight2: NSLayoutConstraint!
    @IBOutlet weak var adImageViewHeight3: NSLayoutConstraint!
    
    var pickedImage  = UIImage()
    var SimilarImagesArray = [String]()
    var SimilarVideosArray = [String]()
    var videoUrl: URL?
    var responseData = Dictionary<String, Any>()
    var fromViewString : String! = ""
    var flagString : String! = ""
    var fromOptionString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.imagesNotFoundLabel.isHidden = true
        self.videosNotFoundLabel.isHidden = true
        
        self.matchedDiagnosisHeaderLabel.text = self.languageChangeString(a_str: "Matched diagnosis")
        self.confirmDiagnosisLabel.text = self.languageChangeString(a_str: "Confirm diagnosis")
        self.mostSimilarImagesLabel.text = self.languageChangeString(a_str: "Most similar images")
        self.mostSimilarVideosLabel.text = self.languageChangeString(a_str: "Most similar videos")
        self.solutiuonsHeaderLabel.text = self.languageChangeString(a_str: "Solutions")
        self.imagesNotFoundLabel.text = self.languageChangeString(a_str: "Images are not available")
        self.videosNotFoundLabel.text = self.languageChangeString(a_str: "Videos are not available")
        if self.fromOptionString == "gender"{
            self.flagString = "1"
        }else if self.fromOptionString == "meter"{
            self.flagString = "2"
        }else{
            self.flagString = "0"
        }
        self.loadScannedData()
        //1.Extreme, 2.Minor, 3.Major,4. Severe, 5.Healthy
        // Do any additional setup after loading the view.
    }
    
    func loadScannedData(){
        if responseData["membership_status"] as? Int == 0{
            print("membership_status == 0")
            self.adImageViewHeight1.constant = 200
            let data = responseData["Ads"] as? [String : Any]
            let image = data?["image"] as? String
            let imageString : String = BASE_PATH + image!
            self.adImageView1.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
        }else{
            print("membership_status == 1")
            self.adImageViewHeight1.constant = 0
        }
        if let data = responseData["Gallery"] as? [String : Any]{
           // let data = responseData["Gallery"] as? [String : Any]
            let image = data["image"] as? String
              let imageString : String = BASE_PATH + image!
            self.scropImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
            self.titleLabel.text = data["title"] as? String
            if self.flagString == "1"{
                self.statusViewHeight.constant = 40
                self.plantGenderOrStatusLabel.text = self.languageChangeString(a_str: "Plant Sexe")
                self.statusValueLabel.text = data["title"] as? String
            }else if self.flagString == "2"{
                self.statusViewHeight.constant = 40
                self.plantGenderOrStatusLabel.text = self.languageChangeString(a_str: "Plant Health Meter")
                self.statusValueLabel.text = data["title"] as? String
            }else{
                self.statusViewHeight.constant = 0
                self.plantGenderOrStatusLabel.text = ""
                self.statusValueLabel.text = ""
            }
            self.diagonisText_lbl.text = data["description"] as? String
            self.solutionLabel.text = data["solution"] as? String
            self.SimilarImagesArray = [String]()
            self.SimilarVideosArray = [String]()
            if let Similar_Images = responseData["Similar_Images"] as? [[String : Any]]{
                for item in Similar_Images{
                    let image = item["image"] as! String
                    let imageString : String = BASE_PATH + image
                    self.SimilarImagesArray.append(imageString)
                }
            }
            
            if let Similar_Videos = responseData["Similar_Videos"] as? [[String : Any]]{
                for item in Similar_Videos{
                    let image = item["video"] as! String
                    let imageString : String = BASE_PATH + image
                    self.SimilarVideosArray.append(imageString)
                }
            }
            
            print("Similar Images count::\(self.SimilarImagesArray.count)")
            print("Similar Videos count::\(self.SimilarVideosArray.count)")
            
            if self.SimilarImagesArray.count > 0{
                self.similarImagesCollectionViewGeight.constant = 130
                self.imagesNotFoundLabel.isHidden = true
            }else{
                self.similarImagesCollectionViewGeight.constant = 0
                self.imagesNotFoundLabel.isHidden = false
            }
            
            if self.SimilarVideosArray.count > 0{
                self.similarVideoCollectionViewHeight.constant = 130
                self.videosNotFoundLabel.isHidden = true
            }else{
                self.similarVideoCollectionViewHeight.constant = 0
                self.videosNotFoundLabel.isHidden = false
            }
            DispatchQueue.main.async {
                self.similarImagesCollectionView.reloadData()
                self.similarVediosCollectionView.reloadData()
            }
        }

    }
    
    @IBAction func showMoreBtnAction(_ sender: UIButton) {
        if sender.currentTitle ==  self.languageChangeString(a_str: "Show more") {
             showMoreBtn.setTitle(self.languageChangeString(a_str: "Show less"), for: .normal);
        }else{
             showMoreBtn.setTitle(self.languageChangeString(a_str: "Show more"), for: .normal);
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        if self.fromViewString == "ads"{
            let story = UIStoryboard.init(name: "Main", bundle: nil)
            let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
            homeVC.modalPresentationStyle = .fullScreen
            self.present(homeVC, animated:true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func cameraBtnAction(_ sender: Any) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
}

extension DetailsVC:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.similarImagesCollectionView{
            return self.SimilarImagesArray.count
        }else{
            return self.SimilarVideosArray.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == similarImagesCollectionView{
             let cell = similarImagesCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SimilarImagesCell
            cell.similaImageView.setKFImage(with: self.SimilarImagesArray[indexPath.row])
           // cell.similaImageView.sd_setImage(with: URL (string: self.SimilarImagesArray[indexPath.row]), placeholderImage: UIImage(named:""))
            return cell
        }else{
            let cell = similarVediosCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SimilarVedioCell
            //cell.similarVedioImageView.image = detailsPagesSimilarVedioArrays[indexPath.row]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.similarVediosCollectionView{
            self.videoUrl = URL(string: self.SimilarVideosArray[indexPath.row] )
            if self.videoUrl != nil{
                let player = AVPlayer(url: videoUrl!)
                let vc = AVPlayerViewController()
                vc.player = player
                present(vc, animated: true) {
                    vc.player?.play()
                }
            }
        }else{
            UserDefaults.standard.set(self.SimilarImagesArray, forKey: "cropImage")
            let showImage = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShowImageViewController") as! ShowImageViewController
            self.present(showImage, animated: true, completion: nil)
        }
    }
}
