//
//  StartVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class StartVC: UIViewController {

    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var limitedAccessLabel: UILabel!
    @IBOutlet weak var languageBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var guestUserBtn: UIButton!
    var window : UIWindow?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.signInBtn.setBorder()
        self.guestUserBtn.setBorder()
        self.addGesture()
        self.signInBtn.setTitle(self.languageChangeString(a_str: "SIGN IN"), for: UIControl.State.normal)
        self.guestUserBtn.setTitle(self.languageChangeString(a_str: "QUICK SCAN"), for: UIControl.State.normal)
        self.limitedAccessLabel.text = self.languageChangeString(a_str: "LIMITED ACCESS")
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String == FRENCH_LANGUAGE {
                let string2 = "En sélectionnant l'une des options ci-dessus, vous acceptez les conditions d'utilisation"
                let range               = (string2 as NSString).range(of: "conditions d'utilisation")
                let attributedString    = NSMutableAttributedString(string: string2)

                //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
                
                self.termsLabel.attributedText = attributedString
                    //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
                self.termsLabel.textColor = .white
            }else{
                let string1 = "By selecting any of the options above, you are agreeing to the terms of service"
                let range               = (string1 as NSString).range(of: "terms of service")
                let attributedString    = NSMutableAttributedString(string: string1)

                //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
                
                self.termsLabel.attributedText = attributedString
                    //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
                self.termsLabel.textColor = .white
            }
            
        }else{
            let string1 = "By selecting any of the options above, you are agreeing to the terms of service"
            let range               = (string1 as NSString).range(of: "terms of service")
            let attributedString    = NSMutableAttributedString(string: string1)

            //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
            
            self.termsLabel.attributedText = attributedString
                //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
            self.termsLabel.textColor = .white
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.isNavigationBarHidden = true
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String ==  ENGLISH_LANGUAGE{
                self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
                self.languageBtn.tag = 1
            }else{
                 self.languageBtn.setTitle("En", for: UIControl.State.normal)
                self.languageBtn.tag = 0
            }
        }else{
            self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
            self.languageBtn.tag = 1
        }
    }
    
    // MARK: Add Gesture Fucntion
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedLabel))
        self.termsLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc func tappedLabel(){
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "AboutVCViewController") as! AboutVCViewController
          blogVC.fromVC = "SignUP"
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        super.viewDidDisappear(true)
       // self.navigationController?.isNavigationBarHidden = false
    }
    
    
    
    @IBAction func languageButtonAction(_ sender: Any) {
        if self.languageBtn.tag == 0{
            self.languageBtn.setTitle("Fr", for: UIControl.State.normal)
            self.languageBtn.tag = 1
            UserDefaults.standard.set(ENGLISH_LANGUAGE, forKey: "currentLanguage")
            self.signInBtn.setTitle(self.languageChangeString(a_str: "SIGN IN"), for: UIControl.State.normal)
            self.guestUserBtn.setTitle(self.languageChangeString(a_str: "QUICK SCAN"), for: UIControl.State.normal)
            self.limitedAccessLabel.text = self.languageChangeString(a_str: "LIMITED ACCESS")
            let string1 = "By selecting any of the options above, you are agreeing to the terms of service"
            let range               = (string1 as NSString).range(of: "terms of service")
            let attributedString    = NSMutableAttributedString(string: string1)

            //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
            
            self.termsLabel.attributedText = attributedString
                //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
            self.termsLabel.textColor = .white
           // self.termsLabel.text = self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
//            self.termsLabel.attributedText = NSAttributedString(string: self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")!,attributes:
//            [.underlineStyle: NSUnderlineStyle.single.rawValue])
                //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
            //self.termsLabel.textColor = .white
            NotificationCenter.default.post(name: NSNotification.Name("reloadTabBar"), object: nil)
        }else{
            self.languageBtn.setTitle("En", for: UIControl.State.normal)
            self.languageBtn.tag = 0
            UserDefaults.standard.set(FRENCH_LANGUAGE, forKey: "currentLanguage")
            self.signInBtn.setTitle(self.languageChangeString(a_str: "SIGN IN"), for: UIControl.State.normal)
            self.guestUserBtn.setTitle(self.languageChangeString(a_str: "QUICK SCAN"), for: UIControl.State.normal)
            self.limitedAccessLabel.text = self.languageChangeString(a_str: "LIMITED ACCESS")
            let string2 = "En sélectionnant l'une des options ci-dessus, vous acceptez les conditions d'utilisation"
            let range               = (string2 as NSString).range(of: "conditions d'utilisation")
            let attributedString    = NSMutableAttributedString(string: string2)

            //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
            
            self.termsLabel.attributedText = attributedString
                //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
            self.termsLabel.textColor = .white
            //self.termsLabel.text = self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
//            self.termsLabel.attributedText = NSAttributedString(string: self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")!,attributes:
//            [.underlineStyle: NSUnderlineStyle.single.rawValue])
           // self.termsLabel.textColor = .white
            NotificationCenter.default.post(name: NSNotification.Name("reloadTabBar"), object: nil)
        }
    }
    
    
    //MARK:- SIGN IN BTN ACTION
    @IBAction func signInBtnAction(_ sender: Any) {
        guestUserBtn.backgroundColor =  UIColor.clear
        signInBtn.backgroundColor = appColor
        UserDefaults.standard.removeObject(forKey: "username")
        UserDefaults.standard.removeObject(forKey: "userId")
        UserDefaults.standard.set(true, forKey: "isGuest")
        
        
//        let homeVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInVC1")
//        self.present(homeVC, animated: false, completion: nil)
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let signVC = story.instantiateViewController(withIdentifier: "SignInVC") as! SignInVC
        self.navigationController?.pushViewController(signVC, animated: true)
    }
    
    //MARK:- GUEST USER BTN ACTION
    @IBAction func guestUsertBtnAction(_ sender: Any) {
        guestUserBtn.backgroundColor =  appColor
        signInBtn.backgroundColor = UIColor.clear
        
        UserDefaults.standard.removeObject(forKey: "username")
        UserDefaults.standard.removeObject(forKey: "userId")
        UserDefaults.standard.set(true, forKey: "isGuest")
//        let story = UIStoryboard.init(name: "Main", bundle: nil)
//        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
//        // homeVC.selectedIndex = 0
//        self.present(homeVC, animated:true, completion: nil)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }
    
    
}
