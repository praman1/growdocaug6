//
//  GalleryDetailsVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import AVKit
class GalleryDetailsVC: UIViewController {
    
    @IBOutlet var imagesNotFoundLabel: UILabel!
    @IBOutlet var videosNotFoundLabel: UILabel!
    @IBOutlet weak var gallaeryImagesCollectionView: UICollectionView!
    @IBOutlet var similarImagesCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet var similarVideosCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet var galleryVideosCollectionView: UICollectionView!
    @IBOutlet weak var galleyImageView: UIImageView!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var solutionLabel: UILabel!
    
    @IBOutlet weak var confirmDiagnosisHeaderLabel: UILabel!
    
    @IBOutlet weak var similarImagesLabel: UILabel!
    
    @IBOutlet weak var similarVideosLabel: UILabel!
    
    @IBOutlet weak var solutionsHeaderLabel: UILabel!
    var selectedImage:String!
    var selectedPlantName:String!
    var galleryIdString : String! = ""
    var SimilarImagesArray = [String]()
    var SimilarVideosArray = [String]()
    var videoUrl: URL?
    var languageString : String! = ""
    @IBOutlet weak var galleryPlantName_lbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagesNotFoundLabel.isHidden = true
        self.videosNotFoundLabel.isHidden = true
        self.confirmDiagnosisHeaderLabel.text = self.languageChangeString(a_str: "Confirm diagnosis")
        self.similarImagesLabel.text = self.languageChangeString(a_str: "Similar Images")
        self.similarVideosLabel.text = self.languageChangeString(a_str: "Similar Videos")
        self.solutionsHeaderLabel.text = self.languageChangeString(a_str: "Solutions")
        self.imagesNotFoundLabel.text = self.languageChangeString(a_str: "Images are not available")
        self.videosNotFoundLabel.text = self.languageChangeString(a_str: "Videos are not available")
        self.galleryDetailsServiceCall()
//        galleyImageView.image = UIImage(named: selectedImage)
//        galleryPlantName_lbl.text = selectedPlantName
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK: GALLERY DETAILS SERVICE CALL
    func galleryDetailsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/gallery_details?id=31&?lang=
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["id":self.galleryIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: galleryDetailsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of galleryDetailsAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let data = responseData["Gallery"] as? [String : Any]
                    let image = data?["image"] as? String
                    let imageString : String = BASE_PATH + image!
                    self.galleyImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                    self.galleryPlantName_lbl.text = data?["title"] as? String
                    self.descriptionLabel.text = data?["description"] as? String
                    self.solutionLabel.text = data?["solution"] as? String
                    self.SimilarImagesArray = [String]()
                    self.SimilarVideosArray = [String]()
                    if let Similar_Images = responseData["Similar_Images"] as? [[String : Any]]{
                        for item in Similar_Images{
                            let image = item["image"] as! String
                            let imageString : String = BASE_PATH + image
                            self.SimilarImagesArray.append(imageString)
                        }
                    }
                    
                    if let Similar_Videos = responseData["Similar_Videos"] as? [[String : Any]]{
                        for item in Similar_Videos{
                            let image = item["video"] as! String
                            let imageString : String = BASE_PATH + image
                            self.SimilarVideosArray.append(imageString)
                        }
                    }
                    print("Similar Images count::\(self.SimilarImagesArray.count)")
                    print("Similar Videos count::\(self.SimilarVideosArray.count)")
                    
                    if self.SimilarImagesArray.count > 0{
                        self.similarImagesCollectionViewHeight.constant = 130
                        self.imagesNotFoundLabel.isHidden = true
                    }else{
                        self.similarImagesCollectionViewHeight.constant = 0
                        self.imagesNotFoundLabel.isHidden = false
                    }
                    
                    if self.SimilarVideosArray.count > 0{
                        self.similarVideosCollectionViewHeight.constant = 130
                        self.videosNotFoundLabel.isHidden = true
                    }else{
                        self.similarVideosCollectionViewHeight.constant = 0
                        self.videosNotFoundLabel.isHidden = false
                    }
                    DispatchQueue.main.async {
                        self.gallaeryImagesCollectionView.reloadData()
                        self.galleryVideosCollectionView.reloadData()
                    }
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}
extension GalleryDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if collectionView == self.gallaeryImagesCollectionView{
            //self.similarImagesCollectionViewHeight.constant = self.gallaeryImagesCollectionView.contentSize.height
            //CGFloat(self.SimilarImagesArray.count * 124)
           // self.similarImagesCollectionViewHeight.constant = height1
            return self.SimilarImagesArray.count
        }else{
//            let height2 = self.galleryVideosCollectionView.contentSize.height
//            self.similarVideosCollectionViewHeight.constant = height2
            return self.SimilarVideosArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == self.gallaeryImagesCollectionView{
            let cell = self.gallaeryImagesCollectionView.dequeueReusableCell(withReuseIdentifier: "imagesCell", for: indexPath) as! SimilarImagesCell
            cell.similaImageView.setKFImage(with: self.SimilarImagesArray[indexPath.row])
            //cell.similaImageView.sd_setImage(with: URL (string: self.SimilarImagesArray[indexPath.row]), placeholderImage: UIImage(named:""))
            return cell
        }else if collectionView == self.galleryVideosCollectionView{
            let cell = self.galleryVideosCollectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! SimilarVedioCell
            
           // cell.similarVedioImageView.sd_setImage(with: URL (string: self.SimilarVideosArray[indexPath.row]), placeholderImage: UIImage(named:""))
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == self.galleryVideosCollectionView{
            self.videoUrl = URL(string: self.SimilarVideosArray[indexPath.row] )
            if self.videoUrl != nil{
                let player = AVPlayer(url: videoUrl!)
                let vc = AVPlayerViewController()
                vc.player = player
                present(vc, animated: true) {
                    vc.player?.play()
                }
            }
        }else{
            UserDefaults.standard.set(self.SimilarImagesArray, forKey: "cropImage")
            let showImage = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ShowImageViewController") as! ShowImageViewController
            self.present(showImage, animated: true, completion: nil)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let yourHeight = yourWidth
        if collectionView == self.gallaeryImagesCollectionView{
            //self.similarImagesCollectionViewHeight.constant = self.gallaeryImagesCollectionView.contentSize.height
           // let yourWidth = gallaeryImagesCollectionView.bounds.width/3.0
            return CGSize(width: 157.0, height: 124.0)
        }else{
           // self.similarImagesCollectionViewHeight.constant = self.galleryVideosCollectionView.contentSize.height
            //let yourWidth = gallaeryImagesCollectionView.bounds.width/3.0
            return CGSize(width: 157.0, height: 124.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
/*
 
*/
