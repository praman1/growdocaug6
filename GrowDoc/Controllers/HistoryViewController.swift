//
//  HistoryViewController.swift
//  GrowDoc
//
//  Created by volivesolutions on 10/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu
class HistoryViewController: UIViewController {
    var userIdString : String! = ""
    var historyArray = [HistoryModel]()
    var sideCheckString : String! = ""
    var languageString : String! = ""
    @IBOutlet weak var scannedImagesHeaderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var historyTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLabel.text = self.languageChangeString(a_str : "History")
        self.scannedImagesHeaderLabel.text = self.languageChangeString(a_str : "Scanned Images:")
        
        self.historyServiceCall()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func historyServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/history?user_id=115
            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
            }
            if UserDefaults.standard.object(forKey: "userId") != nil{
                self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
            }
            let postDict = ["user_id":self.userIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: historyAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of historyAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.historyArray = [HistoryModel]()
                    if let history = responseData["data"] as? [[String : Any]]{
                        for item in history{
                            let historyObject = HistoryModel(histotyData: item as NSDictionary)
                            self.historyArray.append(historyObject)
                        }
                    }
                    DispatchQueue.main.async {
                        self.historyTableView.reloadData()
                    }
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        if self.sideCheckString == "fromSide"{
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension HistoryViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.historyArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.historyTableView.dequeueReusableCell(withIdentifier: "HistoryTableViewCell", for: indexPath) as! HistoryTableViewCell
        let historyObject = self.historyArray[indexPath.row]
        cell.dateHeaderLabel.text = self.languageChangeString(a_str: "Date:")
        cell.dateLabel.text = historyObject.created_date
        let imageString : String = BASE_PATH + historyObject.image
        cell.historyScanImageView.setKFImage(with: imageString)
        //cell.historyScanImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let gallery = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryDetailsVC") as! GalleryDetailsVC
        gallery.galleryIdString = self.historyArray[indexPath.row].gallery_id
        self.navigationController?.pushViewController(gallery, animated: true)
    }
}
