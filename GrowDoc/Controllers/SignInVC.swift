//
//  SignInVC.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu
class SignInVC: UIViewController {

    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var passwordShowBtn: UIButton!
    @IBOutlet weak var rememberBtn: UIButton!
    @IBOutlet var passwordShowHideBtn: UIButton!
    
    @IBOutlet weak var fullAccessLabel: UILabel!
    @IBOutlet weak var onlyEmailLabel: UILabel!
    @IBOutlet weak var weDoNotLabel: UILabel!
    @IBOutlet weak var bySelectingLabel: UILabel!
    @IBOutlet weak var limitedAccessLabel: UILabel!
    
    @IBOutlet var remembermeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var forgotPasswordBtn: UIButton!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var dontYouHaveLabel: UILabel!
    @IBOutlet weak var signUpNowBtn: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var continueAsLabel: UILabel!
    @IBOutlet weak var guestBtn: UIButton!
    
    @IBOutlet weak var infoLabel: UILabel!
    var fromViewString : String! = ""
    var guestCheckString : String! = ""
    var languageString : String! = ""
    
    @IBOutlet weak var line1Label: UILabel!
    @IBOutlet weak var line2Label: UILabel!
    
    
    @IBOutlet weak var bySelectTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var quickScanHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.addGesture()
        self.rememberBtn.setImage(UIImage.init(named: "Group 3974"), for: UIControl.State.normal)
        self.rememberBtn.tag = 1
        self.passwordShowHideBtn.tag = 1
        self.passwordTF.isSecureTextEntry = true
        self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
        let menuVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SideMenuVC") as! SideMenuVC
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: menuVC)
        menuLeftNavigationController.leftSide = true
        
        SideMenuManager.menuPresentMode = .menuSlideIn
        SideMenuManager.menuAnimationDismissDuration = 0.2
        SideMenuManager.menuAnimationPresentDuration = 0.5
        SideMenuManager.menuFadeStatusBar = false
        SideMenuManager.menuShadowRadius = 50
        SideMenuManager.menuShadowOpacity = 1
        self.titleLabel.text = self.languageChangeString(a_str: "Sign In")
        self.userNameTF.placeholder = self.languageChangeString(a_str: "E-mail Address*")
        self.passwordTF.placeholder = self.languageChangeString(a_str: "Password*")
        self.remembermeLabel.text = self.languageChangeString(a_str: "Remember me")
        self.forgotPasswordBtn.setTitle(self.languageChangeString(a_str: "Forgot Your Password?"), for: UIControl.State.normal)
        self.signInBtn.setTitle(self.languageChangeString(a_str: "SIGN IN"), for: UIControl.State.normal)
        self.signUpNowBtn.setTitle(self.languageChangeString(a_str: "SIGNUP FOR FREE"), for: UIControl.State.normal)
        self.infoLabel.text = self.languageChangeString(a_str: "Only username,email and password are needed.Signup to have access to scan history and more.")
        //self.fullAccessLabel.text = self.languageChangeString(a_str: "Sign up to get full access to GrowDoc")
        //self.onlyEmailLabel.text = self.languageChangeString(a_str: "Only email and password are needed for login purposes.")
       // self.weDoNotLabel.text = self.languageChangeString(a_str: "We do not collect any other information that could be used to identify you.")
        
        //self.bySelectingLabel.text = self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
        /*
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            if UserDefaults.standard.object(forKey: "currentLanguage") as? String == FRENCH_LANGUAGE {
                let string2 = "En sélectionnant l'une des options ci-dessus, vous acceptez les conditions d'utilisation"
                let range               = (string2 as NSString).range(of: "conditions d'utilisation")
                let attributedString    = NSMutableAttributedString(string: string2)

                //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
                
                self.bySelectingLabel.attributedText = attributedString
                    //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
                self.bySelectingLabel.textColor = .black
            }else{
                let string1 = "By selecting any of the options above, you are agreeing to the terms of service"
                let range               = (string1 as NSString).range(of: "terms of service")
                let attributedString    = NSMutableAttributedS  tring(string: string1)

                //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
                attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
                
                self.bySelectingLabel.attributedText = attributedString
                    //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
                self.bySelectingLabel.textColor = .black
            }
            
        }else{
            let string1 = "By selecting any of the options above, you are agreeing to the terms of service"
            let range               = (string1 as NSString).range(of: "terms of service")
            let attributedString    = NSMutableAttributedString(string: string1)

            //attributedString.addAttribute(NSAttributedString.Key.link, value: NSURL(string: "http://www.google.fr")!, range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSNumber(value: 1), range: range)
            attributedString.addAttribute(NSAttributedString.Key.underlineColor, value: UIColor.white, range: range)
            
            self.bySelectingLabel.attributedText = attributedString
                //self.languageChangeString(a_str: "By selecting any of the options above, you are agreeing to the terms of service")
            self.bySelectingLabel.textColor = .black
        }
        */
       // self.dontYouHaveLabel.text = self.languageChangeString(a_str: "Don't you have an account?")
        /*
        if self.fromViewString == "guestMem"{
            self.orLabel.text = ""
            self.limitedAccessLabel.text = ""
            self.quickScanHeight.constant = 0
            self.line1Label.isHidden = true
            self.line2Label.isHidden = true
            self.bySelectTopConstraint.constant = 0
        }else{
            self.limitedAccessLabel.text = self.languageChangeString(a_str: "LIMITED ACCESS")
            self.orLabel.text = self.languageChangeString(a_str: "OR")
            self.guestBtn.setTitle(self.languageChangeString(a_str: "QUICK SCAN"), for: UIControl.State.normal)
            self.quickScanHeight.constant = 60
            self.line1Label.isHidden = false
            self.line2Label.isHidden = false
            self.bySelectTopConstraint.constant = 20
        }
        */
        
        //self.continueAsLabel.text = self.languageChangeString(a_str: "Continue as")
        // Do any additional setup after loading the view.
    }
   
    // MARK: Add Gesture Fucntion
    func addGesture() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tappedLabel))
        self.bySelectingLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc func tappedLabel(){
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let blogVC = story.instantiateViewController(withIdentifier: "AboutVCViewController") as! AboutVCViewController
          blogVC.fromVC = "SignUP"
        self.navigationController?.pushViewController(blogVC, animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        //self.tabBarController?.tabBar.isHidden = false
        //self.navigationController?.isNavigationBarHidden = false
    }
    
    
    @IBAction func signInBtnAction(_ sender: Any) {
        if self.userNameTF.text == "" {
            showToastForAlert(message: self.languageChangeString(a_str: "Please enter email address")!)
        }else if self.passwordTF.text == ""{
            showToastForAlert(message: self.languageChangeString(a_str: "Please enter password")!)
        }else if self.userNameTF.text != ""{
            do {
                let email = try self.userNameTF.validatedText(validationType: ValidatorType.email)
                //self.emailIsString = email
                self.loginServiceCall()
            } catch(let error) {
                //showToast(message: (error as! ValidationError).message)
                print(error)
                self.showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
            }
        }
    }
    
    @IBAction func closeBtnAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //    @IBAction func menuBtnAction(_ sender: Any) {
//
//        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
//    }
    
    @IBAction func guestUserBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
        // homeVC.selectedIndex = 0
        UserDefaults.standard.set(true, forKey: "isGuest")
        homeVC.modalPresentationStyle = .fullScreen
        self.present(homeVC, animated:true, completion: nil)
    }
    
    @IBAction func signUpBtnAction(_ sender: Any) {
        let story = UIStoryboard.init(name: "Main", bundle: nil)
        let signVC = story.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(signVC, animated: true)
    }
    
    @IBAction func remeberBtnAction(_ sender: Any) {
        if self.rememberBtn.tag == 1 {
            // UNCheck
            self.rememberBtn.tag = 2
            self.rememberBtn.setImage(UIImage.init(named: "Rectangle 3549"), for: UIControl.State.normal)
        }
        else{
            // Check
            self.rememberBtn.setImage(UIImage.init(named: "Group 3974"), for: UIControl.State.normal)
            self.rememberBtn.tag = 1
        }
    }
    
    @IBAction func forgotBtnAction(_ sender: Any) {
        let alertController = UIAlertController(title: self.languageChangeString(a_str: "Forgot Password"), message: self.languageChangeString(a_str: "Enter email address and send"), preferredStyle: .alert)
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = self.languageChangeString(a_str: "Enter Email Address")
        }
        let saveAction = UIAlertAction(title: self.languageChangeString(a_str: "OK"), style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
             if firstTextField.text == ""{
                self.showToastForAlert(message: self.languageChangeString(a_str: "Please enter email address")!)
             }else if firstTextField.text != ""{
                do {
                    let email = try firstTextField.validatedText(validationType: ValidatorType.email)
                    //self.emailIsString = email
                    self.forgotPasswordServiceCall(emailIdString: email)
                } catch(let error) {
                    //showToast(message: (error as! ValidationError).message)
                    print(error)
                    self.showToastForAlert(message: self.languageChangeString(a_str: "Invalid E-mail Address")!)
                }
            }
        })
        let cancelAction = UIAlertAction(title: self.languageChangeString(a_str: "Cancel"), style: .default, handler: { (action : UIAlertAction!) -> Void in
           // self.dismiss(animated: true, completion: nil)
        })
      
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func passwordShowHideBtnAction(_ sender: Any) {
            if self.passwordShowHideBtn.tag == 1 {
                //SHOW
                self.passwordTF.isSecureTextEntry = false
                self.passwordShowHideBtn.tag = 2
                self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 2347"), for: UIControl.State.normal)
            }
            else{
                //HIDE
                self.passwordShowHideBtn.setImage(UIImage.init(named: "Group 3976"), for: UIControl.State.normal)
                self.passwordTF.isSecureTextEntry = true
                self.passwordShowHideBtn.tag = 1
            }
    }
    
    //MARK:LOGIN SERVICE CALL
    func loginServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/login?device_name=__&device_token=__&nameoremail=(username_or_email)&password=__
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["device_name":DEVICE_TYPE,"device_token":DEVICE_TOKEN,"nameoremail":self.userNameTF.text!,"password":self.passwordTF.text!,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: loginAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of loginAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToast(message: message)
                    let username = responseData["username"]
                    UserDefaults.standard.set(responseData["username"] as! String, forKey: "username")
                    
                    UserDefaults.standard.set(responseData["user_id"], forKey: "userId")
                    if self.rememberBtn.tag == 1{
                        UserDefaults.standard.set(true, forKey: "Loggedin")
                    }else{
                        UserDefaults.standard.set(false, forKey: "Loggedin")
                    }
                    
                    //if self.guestCheckString == "guest"{
                        //UserDefaults.standard.bool(forKey: "isGuest") == true

                        UserDefaults.standard.set(false, forKey: "isGuest")
                   // }
                    let story = UIStoryboard.init(name: "Main", bundle: nil)
                    let homeVC = story.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVCViewController
                    // homeVC.selectedIndex = 0
                    homeVC.modalPresentationStyle = .fullScreen
                    self.present(homeVC, animated:true, completion: nil)
                    
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
        
    }
    
    //MARK:FORGOT PASSWORD SERVICE CALL
    func forgotPasswordServiceCall(emailIdString : String){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/forgot_pwd?email=
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["email":emailIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: forgotPasswordAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of forgotPasswordAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                let message = responseData["message"] as! String
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToast(message: message)
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
}
