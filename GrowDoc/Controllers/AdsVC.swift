//
//  AdsVC.swift
//  GrowDoc
//
//  Created by Apple on 14/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class AdsVC: UIViewController {
    @IBOutlet weak var timesLabel: UILabel!
    @IBOutlet weak var adImageView: UIImageView!
    var userIdString : String! = ""
    var membership_status : String! = ""
    var status : String! = ""
    var seconds = 10
    var timer = Timer()
    var responseData = Dictionary<String, Any>()
    var scannedImage = UIImage()
    var fromOptionString : String! = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.adsServiceCall()
        // Do any additional setup after loading the view.
    }
    
    func runTimer() {
         timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(AdsVC.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1     //This will decrement(count down)the seconds.
        self.timesLabel.text = String(format: "%@ 00 : 0%d %@",self.languageChangeString(a_str:
            "Results will be shown after advertisement. This will close in")!,seconds,self.languageChangeString(a_str: "seconds")!)
        if seconds == 0{
            self.stopTimer()
            if self.status == "1"{
                let  story = UIStoryboard.init(name: "Main", bundle: nil)
                let details = story.instantiateViewController(withIdentifier: "DetailsVC") as! DetailsVC
                self.tabBarController?.hidesBottomBarWhenPushed = false
                details.responseData = responseData
                details.fromViewString = "ads"
                details.fromOptionString = self.fromOptionString
                self.navigationController?.pushViewController(details, animated: true)
            }else{
                 let  story = UIStoryboard.init(name: "Main", bundle: nil)
                 let details = story.instantiateViewController(withIdentifier: "DeseaseNotDetectedViewController") as! DeseaseNotDetectedViewController
                 //details.errorMessage = message
                details.scannedImage = self.scannedImage
                details.fromOptionString = self.fromOptionString
                // details.imageNameString = imageName
                 self.navigationController?.pushViewController(details, animated: true)
            }

        }
        //"\(seconds)" //This will update the label.
    }
    
    func stopTimer(){
        self.timer.invalidate()
        //self.timer = nil
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //MARK:adsServiceCall
    func adsServiceCall(){
            if Reachability.isConnectedToNetwork() {
               // ServicesHelper.sharedInstance.loader(view: self.view)
                //http://demomaplebrains.com/growdoc_phase/services/show_ads?user_id=181
                //            if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
                //                self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
                //            }
                if UserDefaults.standard.object(forKey: "userId") != nil{
                    self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
                }
                let postDict = ["user_id":self.userIdString]
                ServicesHelper.sharedInstance.getDataServiceCall(url: showAds ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                    print("Response of showAds ",responseData)
                    let status : Int = Int(responseData["status"] as! Int)
                    self.membership_status = responseData["membership_status"] as? String
                    if self.membership_status == "1"{
                        UserDefaults.standard.set(true, forKey: "membership")
                    }else if self.membership_status == "0"{
                        UserDefaults.standard.set(false, forKey: "membership")
                    }
                    if let data = responseData["Ads"] as? [String : Any]{
                        let image = data["image"] as! String
                        let imageString : String = BASE_PATH + image
                        self.adImageView.setKFImage(with: imageString)
                        //self.adImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                        self.runTimer()
                    }
                    if status == 1{
                        //ServicesHelper.sharedInstance.dissMissLoader()

                    }else{
    //                    let message = responseData["message"] as! String
    //                    ServicesHelper.sharedInstance.dissMissLoader()
    //                    self.showToastForAlert(message: message)
                    }
                }
            }else {
                ServicesHelper.sharedInstance.dissMissLoader()
                self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
                //self.showToastForAlert(message: "Please check your Internet Connection")
            }
        }
 
}
