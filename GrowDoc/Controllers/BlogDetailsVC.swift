//
//  BlogDetailsVC.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import SideMenu

class BlogDetailsVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var blogContentView: UIView!
    var blogIdString : String! = ""
    
    @IBOutlet var blogImageView: UIImageView!
    @IBOutlet var headerTitleLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var likesLabel: UILabel!
    
    @IBOutlet var commentTextView: UITextView!
    @IBOutlet var commentPlaceHolderLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var commentsLabel: UILabel!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var commentBtn: UIButton!
    @IBOutlet var commentsTableView: UITableView!
    @IBOutlet var commentsTableViewHeight: NSLayoutConstraint!
    var blogImageString : String! = ""
    var noOfComments : Int?
    var noOfLikes : Int?
    var totalLikes : Int?
    var userIdString : String! = ""
    var initialRowHeight : Int? = 0
    var languageString : String! = ""
    
    @IBOutlet weak var postCommentBtn: UIButton!
    @IBOutlet weak var commentsHeaderLabel: UILabel!
    
    
    
    
    var commentsArray = [CommentsModel]()
    var replyCommentsArray = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.commentPlaceHolderLabel.text = self.languageChangeString(a_str: "Add Comments")
        self.commentsHeaderLabel.text = self.languageChangeString(a_str: "Comments:")
        self.postCommentBtn.setTitle(self.languageChangeString(a_str: "POST COMMENT"), for: UIControl.State.normal)
       // mainView.roundedLeft()
        //blogContentView.roundedLeft()
//        let imageString : String = BASE_PATH + self.blogImageString
//        self.blogImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
        if UserDefaults.standard.object(forKey: "userId") != nil{
            self.userIdString = UserDefaults.standard.object(forKey: "userId") as? String
        }
        self.blogDetailsServiceCall()
    }
    
    @IBAction func menuBtnAction(_ sender: Any) {
         //present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.commentTextView.text.count > 0 {
            self.commentPlaceHolderLabel.isHidden = true
        }else{
            self.commentPlaceHolderLabel.isHidden = false
        }
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        //https://apps.apple.com/in/app/adobe-lightroom/id1451544217?mt=12
        if let name = URL(string: "https://apps.apple.com/in/app/growdoc/id1451544216?mt=12"), !name.absoluteString.isEmpty {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            self.present(activityVC, animated: true, completion: nil)
        }else  {
            // show alert for not available
        }
        //        let shareItem : Any = "www.google.com"
        //        let activityViewController = UIActivityViewController(activityItems: [shareItem]  , applicationActivities: nil)
        //        activityViewController.popoverPresentationController?.sourceView = self.view
        //        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func postCommentBtnAction(_ sender: Any) {
        if UserDefaults.standard.object(forKey: "userId") != nil{
            if self.commentTextView.text == ""{
                self.showToastForAlert(message: languageChangeString(a_str: "Please enter comment")!)
            }else{
                self.postCommentServiceCall()
            }
        }else{
            self.showToastForAlert(message: languageChangeString(a_str: "You're a Stay Anonymous,Please create an account")!)
            let signUp = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUp.guestCheck = "guest"
            self.navigationController?.pushViewController(signUp, animated: true)
        }
    }
    
    @IBAction func commentBtnAction(_ sender: Any) {
        if UserDefaults.standard.object(forKey: "userId") != nil{
            if self.commentTextView.text == ""{
                self.showToastForAlert(message: languageChangeString(a_str: "Please enter comment")!)
            }else{
                self.postCommentServiceCall()
            }
        }else{
            self.showToastForAlert(message: languageChangeString(a_str: "You're a Stay Anonymous,Please create an account")!)
            let signUp = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUp.guestCheck = "guest"
            self.navigationController?.pushViewController(signUp, animated: true)
        }
        
       /*
        if UserDefaults.standard.bool(forKey: "isGuest") == true{
            self.showToastForAlert(message: languageChangeString(a_str: "You're a guest,Please create an account")!)
            let signUp = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUp.guestCheck = "guest"
            self.navigationController?.pushViewController(signUp, animated: true)
        }else{
            if self.commentTextView.text == ""{
                self.showToastForAlert(message: languageChangeString(a_str: "Please enter comment")!)
            }else{
                self.postCommentServiceCall()
            }
        }
        */
    }
    
    @IBAction func likeBtnAction(_ sender: Any) {
        
        if UserDefaults.standard.object(forKey: "userId") != nil{
            self.likeOrUnlickeServiceCall()
        }else{
            self.showToastForAlert(message: languageChangeString(a_str: "You're a Stay Anonymous,Please create an account")!)
            let signUp = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            signUp.guestCheck = "guest"
            self.navigationController?.pushViewController(signUp, animated: true)
        }
//        if UserDefaults.standard.bool(forKey: "isGuest") == true{
//
//        }else{
//
//        }
    }
    
    //MARK:BLOG DETAILS SERVICE CALL
    func blogDetailsServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/blog_details?id=1&lang=
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["id":self.blogIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: blogDetailsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of blogDetailsAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let data = responseData["data"] as? [String : Any]
                    let image = data?["image"] as? String
                    let imageString : String = BASE_PATH + image!
                    self.blogImageView.setKFImage(with: imageString)
                   // self.blogImageView.sd_setImage(with: URL (string: imageString), placeholderImage: UIImage(named:""))
                    self.headerTitleLabel.text = data?["title"] as? String
                    self.titleLabel.text = data?["title"] as? String
                    self.descriptionLabel.text = data?["description"] as? String
                    
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "d MMM yyyy"
                    
                    let finalDate: Date? = dateFormatterGet.date(from: (data?["date"] as? String)!)
                    let convertedDate = dateFormatter.string(from: finalDate!)
                    self.dateLabel.text = convertedDate
                    
                    let likes = data?["count_likes"] as? String
                    let comments = responseData["count_comments"] as? String
                    self.noOfLikes = Int(likes!)
                    self.noOfComments = Int(comments!)
                    self.likesLabel.text = likes! + self.languageChangeString(a_str: "Likes")!
                   // let comments = responseData["blog_comments"] as? String
                    self.commentsLabel.text = comments! + self.languageChangeString(a_str: "Comments")!
                    self.commentsArray = [CommentsModel]()
                    if let blogComments = responseData["comments"] as? [[String : Any]]{
                        for item in blogComments{
                            let commentObject = CommentsModel(commentData: item as NSDictionary)
                            self.commentsArray.append(commentObject)
                        }
                    }
                    DispatchQueue.main.async {
                        self.commentsTableView.reloadData()
                    }
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    //MARK:LIKE OR UNLIKE SERVICE CALL
    func likeOrUnlickeServiceCall(){
        if Reachability.isConnectedToNetwork() {
            //ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/blog_likes?user_id=__&blog_id=__&like=(0 for unlike, 1 for like)
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["user_id":self.userIdString,"blog_id":self.blogIdString,"lang":self.languageString]
            ServicesHelper.sharedInstance.getDataServiceCall(url: blogLikesAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of blogLikesAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToast(message: message)
                    let likes : Int = Int(responseData["like_count"] as! Int)
                    let likeCount = String(likes)
                    self.likesLabel.text = String(format: "%@%@", likeCount,self.languageChangeString(a_str: "Likes")!)
                }else{
                    //ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            //ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }
    
    //MARK:POST COMMENT SERVICE CALL
    func postCommentServiceCall(){
        if Reachability.isConnectedToNetwork() {
            ServicesHelper.sharedInstance.loader(view: self.view)
            //https://www.demomaplebrains.com/growdoc_dev/services/blog_comments?blog_id=(blog_id)&user_id=(user_id)&comment=______
        if UserDefaults.standard.object(forKey: "currentLanguage") != nil{
            self.languageString = UserDefaults.standard.object(forKey: "currentLanguage") as? String
        }
            let postDict = ["blog_id":self.blogIdString!,"user_id":self.userIdString!,"lang":self.languageString,"comment":self.commentTextView.text!]
            ServicesHelper.sharedInstance.getDataServiceCall(url: blogCommentsAPI ,postDict: postDict as [String : AnyObject]) { (responseData, error) in
                print("Response of blogCommentsAPI ",responseData)
                let status : Int = Int(responseData["status"] as! Int)
                if status == 1{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToast(message: message)
                    self.commentTextView.text = ""
                    self.commentPlaceHolderLabel.isHidden = false
                    self.noOfComments = self.noOfComments! + 1
                    self.commentsLabel.text = String(self.noOfComments!) + self.languageChangeString(a_str: "Comments")!
                    self.blogDetailsServiceCall()
                }else{
                    ServicesHelper.sharedInstance.dissMissLoader()
                    let message = responseData["message"] as! String
                    self.showToastForAlert(message: message)
                }
            }
        }else {
            ServicesHelper.sharedInstance.dissMissLoader()
            self.showToastForAlert(message: languageChangeString(a_str: "Please check your Internet Connection")!)
            //self.showToastForAlert(message: "Please check your Internet Connection")
        }
    }

}

extension BlogDetailsVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        let height: CGFloat = CGFloat(self.commentsArray.count * 130)
//        self.commentsTableViewHeight.constant = height
        return self.commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.commentsTableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        let commentObject = self.commentsArray[indexPath.row]
        cell.userNameLabel.text = commentObject.userName
        cell.commentLabel.text = commentObject.comment
        cell.dateValueLabel.text = commentObject.created_date
        cell.replyHeaderLabel.text = self.languageChangeString(a_str: "Reply from GrowDoc:")
        cell.dateLabel1.text = self.languageChangeString(a_str: "Date:")
        cell.dateHeaderLabel.text = self.languageChangeString(a_str: "Date:")
        if commentObject.reply == ""{
            cell.replyHeaderLabel.isHidden = true
            cell.replyDateLabel.isHidden = true
            cell.dateHeaderLabel.isHidden = true
            cell.replyCommentLabel.isHidden = true
        }else{
            cell.replyHeaderLabel.isHidden = false
            cell.replyDateLabel.isHidden = false
            cell.dateHeaderLabel.isHidden = false
            cell.replyCommentLabel.isHidden = false
            cell.replyCommentLabel.text = commentObject.reply
            cell.replyDateLabel.text = commentObject.reply_date
        }
        /*
        if indexPath.row == self.commentsArray.count-1{
            UIView.animate(withDuration: 0, animations: {
                self.commentsTableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                // Get visible cells and sum up their heights
                let cells = self.commentsTableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                // Edit heightOfTableViewConstraint's constant to update height of table view
                self.commentsTableViewHeight.constant = heightOfTableView
            }
        }
        */
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        /*
        if indexPath.row == (tableView.indexPathsForVisibleRows?.last! as! NSIndexPath).row {
        var heightOfTableView: CGFloat = 0.0
        // Get visible cells and sum up their heights
        let cells = tableView.visibleCells
        for cell in cells {
            heightOfTableView += cell.frame.height
        }
        // Edit heightOfTableViewConstraint's constant to update height of table view
        self.commentsTableViewHeight.constant = heightOfTableView
    }
        */
        /*
        UIView.animate(withDuration: 0, animations: {
            tableView.layoutIfNeeded()
        }) { (complete) in
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = tableView.visibleCells
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            // Edit heightOfTableViewConstraint's constant to update height of table view
            self.commentsTableViewHeight.constant = heightOfTableView
        }
        */
        /*
        if indexPath.row == (tableView.indexPathsForVisibleRows?.last! as! NSIndexPath).row {
            //End of loading all Visible cells
            let height: CGFloat = tableView.contentSize.height
            self.commentsTableViewHeight.constant = height
            //If cell's are more than 10 or so that it could not fit on the tableView's visible area then you have to go for other way to check for last cell loaded
        }
        */
        
        let frame = tableView.rectForRow(at: indexPath)
//        print(frame.size.height)
    
        let rowHeight : Int = initialRowHeight! + Int(frame.size.height)
//        print("Row Height\(rowHeight)")
        let height: CGFloat = CGFloat(self.commentsArray.count * rowHeight)
        self.commentsTableViewHeight.constant = height
        
    }
}
extension UIView{
    
    func roundedLeft(){
        let maskPath1 = UIBezierPath(roundedRect: bounds,
                                     byRoundingCorners: [.topLeft],
                                     cornerRadii: CGSize(width: 15, height: 15))
        let maskLayer1 = CAShapeLayer()
        maskLayer1.frame = bounds
        maskLayer1.path = maskPath1.cgPath
        layer.mask = maskLayer1
    }
}
