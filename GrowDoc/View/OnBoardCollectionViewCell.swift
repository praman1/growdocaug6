//
//  OnBoardCollectionViewCell.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class OnBoardCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var header_lbl: UILabel!
    
    @IBOutlet weak var headerImageView: UIImageView!
    
    @IBOutlet weak var title_lbl: UILabel!
    
    @IBOutlet weak var description_lbl: UILabel!
    @IBOutlet weak var bottomImageView: UIImageView!
    
}
