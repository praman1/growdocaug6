//
//  BlogTableViewCell.swift
//  GrowDoc
//
//  Created by volive solutions on 29/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class BlogTableViewCell: UITableViewCell {

    @IBOutlet weak var customeView: UIView!
    @IBOutlet weak var blogImageView: UIImageView!
    @IBOutlet var blogDateLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var decriptionLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.customeView.addShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension UIView {
    func addShadow(){
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.50).cgColor
        self.layer.shadowRadius = 3
        self.layer.shadowOpacity = 30
        self.layer.shadowOffset = CGSize(width: 0 , height:2)
    }
}
