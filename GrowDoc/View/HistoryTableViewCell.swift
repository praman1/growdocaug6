//
//  HistoryTableViewCell.swift
//  GrowDoc
//
//  Created by volivesolutions on 10/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {

    @IBOutlet var historyScanImageView: UIImageView!
    
    @IBOutlet weak var dateHeaderLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
