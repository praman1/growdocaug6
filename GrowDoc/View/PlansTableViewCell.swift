//
//  PlansTableViewCell.swift
//  GrowDoc
//
//  Created by Apple on 10/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class PlansTableViewCell: UITableViewCell {

    @IBOutlet weak var membershipNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var noOfScansLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var customNameLabel: UILabel!
    @IBOutlet weak var supportLabel: UILabel!
    
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var backImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
