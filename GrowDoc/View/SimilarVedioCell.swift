//
//  SimilarVedioCell.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class SimilarVedioCell: UICollectionViewCell {
    
    @IBOutlet weak var similarVedioImageView: UIImageView!
}
