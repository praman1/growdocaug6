//
//  ReplyTableViewCell.swift
//  GrowDoc
//
//  Created by volivesolutions on 05/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class ReplyTableViewCell: UITableViewCell {

    @IBOutlet var replyCommentLabel: UILabel!
    
    @IBOutlet var replyDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
