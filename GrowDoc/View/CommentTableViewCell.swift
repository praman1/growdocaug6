//
//  CommentTableViewCell.swift
//  GrowDoc
//
//  Created by volivesolutions on 29/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet var userNameLabel: UILabel!
    
    @IBOutlet var dateHeaderLabel: UILabel!
    @IBOutlet var dateValueLabel: UILabel!
    @IBOutlet var commentLabel: UILabel!
    @IBOutlet var replyDateLabel: UILabel!
    @IBOutlet var replyCommentLabel: UILabel!
    @IBOutlet var replyHeaderLabel: UILabel!
    
    @IBOutlet weak var dateLabel1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
