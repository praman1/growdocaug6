//
//  BannerCollectionViewCell.swift
//  GrowDoc
//
//  Created by volivesolutions on 13/12/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var bannerImageView: UIImageView!
}
