//
//  SimilarImagesCell.swift
//  GrowDoc
//
//  Created by volive solutions on 30/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class SimilarImagesCell: UICollectionViewCell {
    
    @IBOutlet weak var similaImageView: UIImageView!
    
    @IBOutlet var similarVideoImageView: UIImageView!
}
