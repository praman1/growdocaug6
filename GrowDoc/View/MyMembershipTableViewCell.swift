//
//  MyMembershipTableViewCell.swift
//  GrowDoc
//
//  Created by Apple on 13/07/20.
//  Copyright © 2020 RamiReddy. All rights reserved.
//

import UIKit

class MyMembershipTableViewCell: UITableViewCell {

    @IBOutlet weak var membershipNameLabel: UILabel!
    @IBOutlet weak var buyNowBtn: UIButton!
    @IBOutlet weak var backImageView: UIImageView!
    
    @IBOutlet weak var buttonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tick1ImageView: UIImageView!
    @IBOutlet weak var tick2ImageView: UIImageView!
    @IBOutlet weak var tick3ImageView: UIImageView!
    @IBOutlet weak var tick4ImageView: UIImageView!
    @IBOutlet weak var tick5ImageView: UIImageView!
    
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var selectedMembershipLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var expireDaysLabel: UILabel!
    @IBOutlet weak var scansLeftLabel: UILabel!
    
    @IBOutlet weak var supportLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
