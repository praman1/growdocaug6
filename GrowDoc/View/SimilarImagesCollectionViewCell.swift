//
//  SimilarImagesCollectionViewCell.swift
//  GrowDoc
//
//  Created by volivesolutions on 30/11/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit

class SimilarImagesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cropImageView: UIImageView!
    
}
