//
//  AppDelegate.swift
//  GrowDoc
//
//  Created by volive solutions on 28/10/19.
//  Copyright © 2019 RamiReddy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var badgeCount : String? = ""
//volivesoluitons.GrowDoc
    
    //Live:com.GrowDoc
    //pod 'MaterialComponents/PageControl'
//    let pageControl = MDCPageControl()
//    let pages = NSMutableArray()
    
    /*
     in view did load
     let pageControlSize = pageControl.sizeThatFits(view.bounds.size)
         pageControl.frame = CGRect(x: view.bounds.width/2 - pageControlSize.width/2, y: newsCollectionView.frame.origin.y + newsCollectionView.bounds.height - 20, width: pageControlSize.width, height: pageControlSize.height)
     6:54
     in view did load
     pageControl.addTarget(self, action: #selector(didChangePage), for: .valueChanged)
         pageControl.autoresizingMask = [.flexibleTopMargin, .flexibleWidth, .flexibleLeftMargin, .flexibleRightMargin]
         view.addSubview(pageControl)
     6:54
     @objc func didChangePage(sender: MDCPageControl) {
         var offset = newsCollectionView.contentOffset
         offset.x = CGFloat(sender.currentPage) * newsCollectionView.bounds.size.width
         newsCollectionView.setContentOffset(offset, animated: true)
       }
     
     @objc func didChangePage(sender: MDCPageControl) {
       var offset = newsCollectionView.contentOffset
       offset.x = CGFloat(sender.currentPage) * newsCollectionView.bounds.size.width
       newsCollectionView.setContentOffset(offset, animated: true)
     }
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
       pageControl.scrollViewDidScroll(newsCollectionView)
     }
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
       pageControl.scrollViewDidEndDecelerating(newsCollectionView)
     }
     func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
       pageControl.scrollViewDidEndScrollingAnimation(newsCollectionView)
     }
     // Private Functions
     private func viewConfigrations() {
       newsCollectionView.decelerationRate = UIScrollView.DecelerationRate.fast
     }
     */
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //UserDefaults.standard.removeObject(forKey: "currentLanguage")
        IQKeyboardManager.shared.enable = true
        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: "YOUR_CLIENT_ID_FOR_PRODUCTION",
        PayPalEnvironmentSandbox: "Aa-sEeZBg-invTlUFuF-hwbgzZnTpKiqT9Md6cphe27K0VqHsas_EkMD942EclwmbfiLoYEb3RnoYgp5"])
        Thread.sleep(forTimeInterval: 2.0)
//        let badge : Int = Int(self.badgeCount ?? "")
//        UIApplication.shared.applicationIconBadgeNumber = badge
        if UserDefaults.standard.bool(forKey: "Loggedin") == true {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let initialViewController = storyboard.instantiateViewController(withIdentifier: "TabBarVC")
            //let navigationController = UINavigationController(rootViewController: initialViewController)
            //navigationController.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            self.window?.rootViewController = initialViewController
            self.window?.makeKeyAndVisible()
        }else{
            if UserDefaults.standard.bool(forKey: "onboardFirst") == true{
                
                let rootViewController = self.window?.rootViewController as! UINavigationController
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mvc = storyboard.instantiateViewController(withIdentifier: "StartVC") as!
                StartVC
                rootViewController.pushViewController(mvc, animated: false)
               /*
                self.window = UIWindow(frame: UIScreen.main.bounds)
                
                let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
                let viewController = storyboard.instantiateViewController(withIdentifier: "StartVC") as! StartVC
                let navigationController = UINavigationController.init(rootViewController: viewController)
                self.window?.rootViewController = navigationController
                
                self.window?.makeKeyAndVisible()
                */
//                let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//                let initialViewControlleripad : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "StartVC") as UIViewController
//                if let navigationController = self.window?.rootViewController as? UINavigationController{
//                    navigationController.pushViewController(initialViewControlleripad, animated: true)
//                }
//                else{
//                    print("Navigation Controller not Found")
//                }
            }
        }
        if #available(iOS 10.0, *){
            UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert], completionHandler: {(granted, error) in
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
            //UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }else{
            //UIUserNotificationType.badge,
            let type: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
            print("User Notifications else")
        }
        
//        if Reachability.isConnectedToNetwork(){
//            self.isAuthorizedtoGetUserLocation()
//        }else{
//            print("No Internet")
//        }
        // Override point for customization after application launch.
        return true
    }
    
//    func isAuthorizedtoGetUserLocation() {
//        locationManager.requestAlwaysAuthorization()
//        locationManager.startUpdatingLocation()
//        if CLLocationManager.authorizationStatus() != .authorizedWhenInUse  {
//            locationManager.requestWhenInUseAuthorization()
//        }
//    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        print("didRegister")
        application.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // Convert token to string
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        // Print it to console
        print("APNs device token: \(deviceTokenString)")
        let defaults = UserDefaults.standard
        defaults.set(deviceTokenString, forKey: "deviceToken")
        // Persist it in your backend in case it's new
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        // Print the error to console (you should alert the user that registration failed)
        print("APNs registration failed: \(error)")
    }

}

@available(iOS 10.0, *)
extension AppDelegate: UNUserNotificationCenterDelegate{
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Swift.Void){
        let userInfo = notification.request.content.userInfo as! Dictionary <String,Any>
        
        print("Notification data willPresent is:\(userInfo)")
        let aps = userInfo["aps"] as? Dictionary<String,Any>
        let infoDict = aps!["info"] as? Dictionary<String,Any>
        
        completionHandler(UNNotificationPresentationOptions(rawValue: UNNotificationPresentationOptions.RawValue(UInt8(UNNotificationPresentationOptions.alert.rawValue)|UInt8(UNNotificationPresentationOptions.sound.rawValue))))
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void){
        
        let userInfo = response.notification.request.content.userInfo
        print("Notification data didReceive is:\(userInfo)")
        let aps = userInfo["aps"] as? Dictionary<String,Any>
        let infoDict = aps!["info"] as? Dictionary<String,Any>
        let type = infoDict!["type"] as! String
        if type == "1"{
            //Gallery
            let galleryId = infoDict!["gallery_id"] as! String
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if  let details = storyboard.instantiateViewController(withIdentifier: "GalleryDetailsVC") as? GalleryDetailsVC,
                let tabBarController = self.window?.rootViewController as? UITabBarController,
                let navController = tabBarController.selectedViewController as? UINavigationController {
                details.galleryIdString = galleryId
                navController.pushViewController(details, animated: true)
//                let badge : Int = Int(self.badgeCount)!
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }else if type == "2"{
            //Blog
            let blogId = infoDict!["blog_id"] as! String
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if  let details = storyboard.instantiateViewController(withIdentifier: "BlogDetailsVC") as? BlogDetailsVC,
                let tabBarController = self.window?.rootViewController as? UITabBarController,
                let navController = tabBarController.selectedViewController as? UINavigationController {
                details.blogIdString = blogId
                navController.pushViewController(details, animated: true)
//                let badge : Int = Int(self.badgeCount)!
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
            /*
            let blogId = infoDict!["blog_id"] as! String
            let rootViewController = self.window?.rootViewController as! UINavigationController
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let details = storyboard.instantiateViewController(withIdentifier: "BlogDetailsVC") as!
            BlogDetailsVC
            details.blogIdString = blogId
            //self.chatTypeStr = "appChat"
            rootViewController.pushViewController(details, animated: false)
 */
        }
        completionHandler()
    }
}


